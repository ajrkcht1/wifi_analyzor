#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Installing all necessaries modules.
"""
import sys  # Encoding=utf8
import os
import pip


def install(package):
    pip.main(["install", package])


def before_installing():
    reload(sys)
    sys.setdefaultencoding('Cp1252')
    os.environ['PYTHONIOENCODING'] = 'utf_8'


def main():
    before_installing()
    packages = ["datetime",
                "elasticsearch",
                "jsonpickle",
                "wmi",
                "https://github.com/savon-noir/python-libnmap/zipball/master",
                "https://bitbucket.org/wbruhin/objectlistview/get/tip.zip"]
    map(install, packages)
    # Check imports:
    from datetime import datetime
    from elasticsearch import Elasticsearch
    import jsonpickle
    import wmi
    import libnmap
    import ObjectListView

if __name__ == "__main__":
    main()