from scapy.all import *
import sys
sys.stdout = sys.__stdout__     # Fix scapy's stdout.


def print_url(packet):
    print (str(packet[Raw])[str(packet[Raw]).find('Host: ') + len('Host: '):str(packet[Raw]).find('\r\n', str(packet[Raw]).find('Host: ') + len('Host: '))]) + (str(packet[Raw])[str(packet[Raw]).find('GET ') + len('GET '):str(packet[Raw]).find('HTTP')])


def http_get_filter(packet):
    return TCP in packet and Raw in packet and str(packet[Raw]).startswith('GET')


def clients_packets_filter(packet):
    if http_get_filter(packet):
        pass


def sniff_and_call_func(func):
    sniff(lfilter=http_get_filter, prn=func)

#_________________Main__________________
if __name__ == "__main__":
    try:
        packets = sniff(lfilter=http_get_filter, prn=print_url)
    except KeyboardInterrupt:
        print "Stopped."
    raw_input("Enter any key to Exit...")
