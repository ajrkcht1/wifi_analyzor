import os
import socket
from sniff_http import sniff_and_call_func


class Client(object):
    MAX_LEN = 8
    RECEIVE_BUFFER = 2048

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.socket = socket.socket()

    def connect(self):
        self.socket.connect((self.ip, self.port))

    def disconnect(self):
        self.socket.close()

    def send(self, data):
        data_len = len(data)
        total_sent = 0
        while total_sent < data_len:
            sent = self.socket.send(data[total_sent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            total_sent = total_sent + sent

    def receive(self, message_len=len("ACK")):
        chunks = []
        bytes_received = 0
        while bytes_received < message_len:
            chunk = self.socket.recv(min(message_len - bytes_received, Client.RECEIVE_BUFFER))
            if chunk == '':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_received += len(chunk)
        return ''.join(chunks)

    def send_message(self, data):
        self.send(self.get_len(data))
        self.send(data)

    @staticmethod
    def get_len(data):
        len_data = len(data)
        st = ""
        for i in range(Client.MAX_LEN - len(str(len_data))):
            st += '0'
        return st + str(len_data)

    def send_packet(self, packet):
        print "Packet was sent!"
        str_packet = packet.payload.load
        self.send_message(str_packet)


def start_wmi_service():
    start_wmi_service_command = r"sc start winmgmt"
    set_firewall_settings = r"netsh advfirewall firewall set rule group=\"windows management instrumentation (wmi)\" new enable=yes"
    os.system(start_wmi_service_command)
    os.system(set_firewall_settings)


def get_name():
    return socket.gethostname()


def main():
    MANAGER_IP = "127.0.0.1"
    DISCOVER_KNOWN_SERVER_PORT = 8888
    HTTP_SERVER_PORT = 8889
    start_wmi_service()

    client = Client(MANAGER_IP, DISCOVER_KNOWN_SERVER_PORT)
    client.connect()
    name = get_name()
    client.send_message(name)
    ack = client.receive()
    print ack
    client.disconnect()

    client = Client(MANAGER_IP, HTTP_SERVER_PORT)
    client.connect()
    sniff_and_call_func(client.send_packet)
    raw_input("Press <Enter> to Exit...")

if __name__ == "__main__":
    main()