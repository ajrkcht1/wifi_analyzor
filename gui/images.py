from wx.lib.embeddedimage import PyEmbeddedImage

Apple16 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHUlEQVR42o2TvWoCQRSF/Uth"
    "IzapLGyFQCwWtDalILZRi7yAeQElRSCPIWpjZSE2ImhjYRFIYx9SRF/ANEHdqAdyLhyGRXfh"
    "4+7MvefM3ZnZSNDjeZ4RY2yAGUhxHA0WMElhguMmOIEPy2u9K4w7K2fBDw2qnEtIXUTFJsqA"
    "PN8L4B1UwC24B0ntQFtKgR7YccUN6IIXMAE+57/Aoy2sHcxZ8MfoorlPkNZPqDGxA0cpPhBf"
    "xFuQs70ygw6TB0YXzfVtM7WDMZN+CIO3IIPBlQ7UfETNjRq0QhgcJZaoi5tBUYtCmPyCunsP"
    "FtrqBfaMT2Zg1/LBjo/4eqTExFO9SGryaoIL7a9BVu+B++u2wTcYgrL3/zyDFViCO60/A64j"
    "GmY32TDSAAAAAElFTkSuQmCC")

#----------------------------------------------------------------------
Apple32 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACa0lEQVR42rWWP2hTURTG88dE"
    "hahYpKBIsbhkbQMFWyqGSHCuLdXFDilkKHQIdEldujlJq0uXdijiKE5OgiBY6OBkXcSCINpF"
    "qtAWY176Xr/he/hx+nhPw03gxw333XfOd849576b6vZXKpWULMcCqIOn4DTn0jFmnDq/C76A"
    "AHwFZ3stQJ0/pGOP49vQOXDu3zqfEOct/m/y2aneCGBkIAc+0ukfjt9Bv2Sg+xSLkayQluir"
    "IGDkPv9P8lle1qdIV45TFqnwRTrtgENwn/MZW6hmLtF5Rl44D25yr8ugX57VwTb3/CLnBsBt"
    "rr8l8zagxLa6BB6DHyCQFO+DV2AGDLLdLoAHrP7fZv1PsAqumOzEttUo+CaGjkCHYyC0wR6f"
    "ybyspxAGUlER6lyV3QAH4sA3xn0at049zkWtb0sgCE5EaLtwv3b0UEnAFwJFkcytgHPq06Z+"
    "SSIPXCCZemTTbw+VPPikih0Q2nkftqItQo1+WF50Hf2sHs2aAZ28py85wJcsFE36IwXMS/G5"
    "FLAH+sLCixPQ6JGAfXD5XwTUVIBjEWNRW2CL8I7LDiCeacGcLUI9Aa/LN93vQR1c004gf88B"
    "jh8cZ0FtvQFnTtwTjaoV13VgWnsLFOOO4nFNnWP0y1gO/YYClHeut8EE9RkUdCtsFqZM2lx3"
    "RF39qQAtyNcORaidTf0YxV1KhkDLUT34pA1G/uda1ki4FXkWu87cLRZMx8XfijmumesWSah2"
    "rpN9Xzd2rYDIWshwXNZU8v8v8BzMgWlQ4813N2LtE7VnnMeLkO2ogA3wEjTBVVmjYx8/6y/A"
    "M1CVbY10fgx7rxoLPG0gLAAAAABJRU5ErkJggg==")

#----------------------------------------------------------------------
Computer16 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA00lEQVR42pXRoQrCYBQF4FsE"
    "32NBNPgKC4bFFZXlGdZM+gRm0VcwGW3GhfkKahBBBE0ismAR5+8JNxzEn+0/8IV5d+7gKkgd"
    "IkgcRdqVNeRwcpRrVz4QwG8G4Ik9gXbFgA+cFrxhKvb4YGwLlvCCg+sC/vpQZ40qC2I6zh02"
    "+tIRRrDQGYt5Qch/ER1vDhc91hgSElpvQOnofOVyA04NMmiXLUhh5ijVrjzAOHrCRLvSh7M+"
    "sIIKxZ/5VrvW7GnBTirGA1/1IFFd+r0J1lzBlLhx4Qtdm3fHuPUOrAAAAABJRU5ErkJggg==")

#----------------------------------------------------------------------
Computer32 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABhUlEQVR42s3VsUsCYRgG8HeI"
    "oqUhImoKaSmhG11cpOWW4P6F9uYWIfQPEFwchIYb2oJaHRwOgghdrmhMuCBoCwQpIY2vZ3iD"
    "d7g+z/PurQd+ePeB9z4vitJ/SQHa0IcwZ32eVSDOAQzBKBvybOqKwx4EOeuJeV2gCd/4lDxr"
    "tFh8MDybDKtRstTAQINSh58B8xbYgg/xGS5pF2iCEQ7VCsjthaZmAbn9G78OtArI7e+gCobt"
    "axSQ27vgiPvTLAuUIYgxFtv/5JnPbsTP+RUEM5RtBTwwFq4o0OKzKaxDB0wCnq1ABcJfNEDG"
    "BcNa4voFQouKpcBcWYYRGGECu9l+Ce25BiP4QJoFji3bqxTYhC+5/SIF6pQuJ3AB25Q89bgC"
    "D1CEnZwVeZYo8LdoqjhsCm14Ffd0q1jgSf638GwqwbtiiUf45Jkl4jhwCQOIZhhbHj5O8P4B"
    "z3IoZQJLgYAUcm4p0CaFrMIReBKfrVCG2YAq1BZ0BnuUIh0wGbmnFIkyLBBBbL4BT03fn4fH"
    "NksAAAAASUVORK5CYII=")

#----------------------------------------------------------------------
Linux16 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACwklEQVR42o3RS0hUURgH8P+5"
    "c2fGq+Po+JyQ1BY+QkdNi5lMXbgwRJJqCKRpERU96EEoQQXuIhdSGS6cSREMhEwCKaMXlET4"
    "SLQya8zCR9DDRWqa0+R9fJ3r0EYS/MF3FufjcL4HVktISEBWVhYcDoeB2240Gp0ABEmSIIoi"
    "1oOtHIy1AiCLxUJWq7XV5/PB5/OydT3m7HZ74qIUbqHc3FxFksIIQDFCDFiL1RolICT54KHj"
    "gZOHd9G1huvylgInAfAgZO0+4qPDGGADEB3fWB01R29M5O+MXs7LtBGAGoDBbDaL+B+eQIRk"
    "ZAiJa6wtnp3siqXA2Dl5394KMoimF0ARqqqqBD7cVY0zhuzsbHg8HkNlZQVSU1Mu9798RdPT"
    "XxXienp6VADEt3BizTnw5L/LErfbTTpVUyn4e4n8fr/G10oAFvk6MxAiQDfVAURHMjhdRQYp"
    "woZwKewW/5E4WVEU0kPn8zbJAMhkMrZgBTOY9Y4/tgERYYKAkMyi4tIl4gKBgCbLMqmqSrre"
    "3j6NMRAv9IsgsEhwBgEMDWWAp1wUywuASheOXjhTRpy8sDBPwWCQNE0j3WBvtwaYyGwOnynK"
    "YzEIYfjxUAR9zhcnbqKURpPffnu+gS7VHlNn5wP8d5lkWSVN4XO4k6ZlJoI6623zRJmJRBkI"
    "vk5kmGgHxtuQtDyQFFwe2ko05VQba0D1V2+oeiV6jPvHlA8dEi/DqqjvcolG8rrUfhi1gSjg"
    "+z1gqBXpyqBdoVEH0XuXRuPJWn/3aXr0pI+avF4qzE+nqbtWjaZ3aH8GHfSrN+uZMpxqWR7c"
    "BLTXAWcPsJjuOtQ+vSI8/nTbMjP3QKKLbgwDaObhTYnB/dEWozbZaQk01xirAZjAOTPAsEp4"
    "jh3Z53diT2Hqxrj92+JBNC/E2tNsp0qw+4gLLiJgpDtHpJ+bGVE+/gJ7WE2C4mj1lwAAAABJ"
    "RU5ErkJggg==")

#----------------------------------------------------------------------
Linux32 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAHB0lEQVR42rVWC1BU5xU+d/de"
    "dmWXlyhPERGLiA8UeUhJtUrsAK1xrLUtMq1NjEPsmHYo0Wli+prYB2NTQ5OmxhBSm1pMrCaT"
    "NJSkVVsTMRAeu7xkTEWlNE6LSXyQLrt77//1/Bd2hYlMGwPfzMe53Pnvf757/u+cu3QbsIxw"
    "PEJCQgL3pxTKaFSZa5jfZe5ifoapBDk1CL5dMrOFCaaQTEhIQGRk5Am+TggInfzsIyBFUV7m"
    "KJP7mH7J3Nxcf1xcnLx3CoAyBSKCm8WwgKuBt+drGQP0jcZ1NALrVJQ/w263Iz8/HxERYWbi"
    "VatWoqysjP+PkNUAi3oi4JOpELCkpKQYZ8/24AvrNmBBxlK4XG50d7mweXOpLgWoqlo/lRVI"
    "LywsFMUlG/DDyvWiavddKNu8CTl5q1Fd/UtTALfjS1MpIClraZbn6N4FuH6ShGh24K0awsm6"
    "u7B501pTgKappwO+YUy2CZXp24voPbyjwHCFCtEWCb1zDuCyorxEtqQGTVXel2ad1E6wWq2B"
    "jaaVFCT3d/82AkYjGR/+zYpLLxDePV6AL34+D3HxSfqKFXkgorLR1lUnS0DgCFLXFm/wvHL4"
    "Kby6d4Zoqp0F919+Cr8BvHH6DJ+/ZsycGSMFNEVHR1s0TfvkFWBXk9PpDBjq99XV+8DQn3/h"
    "FbS29UIXgGH4IVFaWiqT6yxYxgcmpR3ZSIHk35H9z9BhQmAEBnR95FZ9fb05oEYn5TAzN2Di"
    "T+R+nvNZHLyVlZVmZr/fj2vXrgUTDw4OoqmpCd3d3cjIyJDJjVERLumb2zKkbKGwsLCA8pcd"
    "DgeOHz+uCyEgOTw8bEaJoaEh9Pb2orGxERUVFZBHwOvNycj8wW3NBRZgJucRu4SDvmPHDjDM"
    "t54ILMisQkpKCrhqgv0jBVzhasR/7Cqw8VSeasTcExsbi76+Pj8gDWdMmFwejcSWLVtkYsTE"
    "xAS+Dzs/liEVZURpZuYiB4fzRUVFZu5Aco/HE0g2FsF7VVVVpoCoqChdRubp/7sCFWU3z2ua"
    "Xd2pqlbs2fOIDobP54PEwMCAFDGhgNpnakwB4eHhYqQCJKfjjP8p4liFLJISXMQD8Az/xdM1"
    "B42xAi5evAiv1zuxgGd/Ywrgowv8ZviPqiqzrFbezXKLlnTvJ/oze/XFB9gxDaTikl0FYun1"
    "X9neOFZF+N5D39ZHDBgUYHbBeLAHzFX9eHrfvWAPIyLcYcTGJSLcaWuzLOslOgWKcNLE2J41"
    "jdCjWOEm4hl/r96VDFxeZvyBRWzd+k0Awba7ZTf4ZAHOrUbDo3IGKEwSGXM0XD6RdBlDCTOB"
    "RPK6ohS9ZQ6NQ/uTRIfuI3puG4X8+4/Ktv4jdPT6CdUj3EvgbVkBXErGlmJCdt5aXP3gStD1"
    "jHHX5/8+gP76RPTVEdYvs2JlGonmWg3onYn3Xw9b5D1lI/3tWFW40km0z6Mg+AHLuYNEPc/Q"
    "cm9jONAxV6Aj3YA7G8K1HOhcjuvN4UiYTpidPA8fDt0IJh7bmn3nB9BSG8PPE268ZoHnJAHd"
    "aYZw5UC0zW/4oIHsVxuIfE3x443o+jVZ3qwi+usjNNfzptODzjz5kICbk7uzoLflAufTceTn"
    "hMWZd+DK4L8QMCUbL3gcv3uuDi99n4AeBwzXbAiTco/lBpOvF3eL1sQU0ZpEom3uTTNef40s"
    "gy+ygF/Qp7xnIobhzoMUIFzZCIiQ93wtCgZcP4YJoUMw5JWh6+LJ/U9hRlgomh6XAlKht+fL"
    "PZjZAp25Bjp5L1emYbTMzhGtKSRaU28KAMjCJPdh2oh2DeiabaAnXaA7U0dHDlcjVxi8Ibqi"
    "cfYQGbu+Va4fqKlF9WP7UF5ejnmpc822S4u1GO8eU3hdOifOAyflfZYKX3Ma/lmfXH9y/4wF"
    "nc8nUv+rycquUoXGCpCw7L6bFnY8S3/qrqWrFw4T3qu3wP+WkzdKhOhM1OHS/JcOKciIIzOh"
    "RSGDo4fjDSKrP2eOgn/UkSHaw/3oSPJ7G6fj3KGQK1X30P1EFFWYG2Lb/qVI646vzqRtG6M+"
    "MoAtIRYK5Y2iozXrivWLaevD6+mxI7vobfaI70Idia4DhIP30TsJ0+2VdkfYuqjI8GL+6Kx2"
    "hto+R0RfyUykR+sfJL1jPwkmju6ks5mxdCfvGT8vyZ6wJttm37jGQTkLQyl/sUpB3P9lotoH"
    "iR76GlkKP01Oi0bRLChOtVpmEalpOYnWoj0blB/9bCP9ZGk8ZRBRaFQo2UZ/rIQxE1UrpXJM"
    "3VZA9+zdpDz+cImyOzVSXUiaGpM2S5FjWAuVC+Lp1qi5m6ggjeiz84m+cQcLWUi21YsobEEK"
    "RZCFpqVqCn29gGhlBmmpMRQyZqazDtI4OmwaRYbyB3S+k+jCAaKc+WRbxevRPHLMNpU+gv8C"
    "8Wu68uZfeh8AAAAASUVORK5CYII=")

#----------------------------------------------------------------------
Microsoft16 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVR42o3QTWsTURTG8efe"
    "m7hx103p0mQtqYigK6Gzk0pN8TMUXOhCRCiCrYK4iWA+Q8ZFwI1UJROpxBLiTtBVP4M1b9Yu"
    "Jpn0+mcYm5EgeODHYZh7nntm9K/aaf/FwKGQMRAo6uet0plxtWSON0pucPuCvd+Te75nc4fz"
    "tThsMkI68Hh++zKu4x5e4zIEOw/ZSJlRtbTM842Tm+Vr2x092410yMFjeCRZDyA4cdhCqOAQ"
    "I3g+pf6gq96TlmFAs0wMj7V8gIMQwCPOeo2AThYQI8FkIWBcLTsIATxi+NFmuf6QDZ4SsMvt"
    "yPcAgtNsfcVCqGAfb/ApWV/ZuttV7VFLH7fbeot32MM+KhCs1JxaCKs4MM3JezWTz/Q7vq2X"
    "06jY9ZGJ0EYLB1iFYOUafQchgC80+rFtDHyxcVT3H9SbREV/GpkZ8j2A4GTCoYMQwNtwGCvt"
    "gxoBnSkB3BQjwQQeaxDSAAvhIr4x+EPhyLuwn26QBcxwinghQFQa8Gok8+KLiuHREiGBGidX"
    "+Ac7s8h95eAQaVDWg/wGf5iMIF7mLeEqthDiEgQ73yDjwoFROHbnwu+WT3C/WudNPuy/Kz8E"
    "A4dC5iz0N/kGraIfFHadAAAAAElFTkSuQmCC")

#----------------------------------------------------------------------
Microsoft32 = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAADAUlEQVR42u3Uz2sUdxjH8Wdm"
    "p7WHKPQkPRib+IOkxaV6UISC1HpVVmmhBaFQWkrVpicFjVl3BENvBVEK6kFq0JNgMKHMRkiR"
    "IkhP/QdKQTw0PzZaLbvszOzT96zP4Us2TEzYFEr9wotnd2Hn85mH2ZVXp9vnXFVcHnwENgVr"
    "GlZA4AZ2LfSvUr/8fXCLPD7aKyqjcvae3RksXDJlVAzv16OIYzgAgY9lw1weCgiMB8mEP3lu"
    "2Abswme4iPuYQROKEIIAywb7i8OeHOoTHdghty71eF/91r7QIL7EZfyCWehiToERp0D+eVrq"
    "k2eEPi9tycLfxG58gR+eHu5/qFvfe+vMtExWovaFE6hpITYJUqdAJa+Ae+eCvbiOB6hB0YJS"
    "QPXdYu/pablrBRpILFyXEK+kQGAzhCJ2wmMkFGjpYHETBSY6NtDFAmUoGhaugG3gnbUvUHE3"
    "YFr/qwJlKOqITdOegcQKjDsPYZyjDkU5t0Dt8LZMYDOEIoa65o9sUy3u3DzMz/A8BUI2AM0R"
    "2wwhCNB59APsFx+CPTiFExhytT6Ub3WjrP/8Zzl0PJKTx6ryDYZynMAp7IHA/5rZecZV5I76"
    "EOzDKIYx4vLvpGW5rRs0kk/SqHBBI+8sRnIMYxT7IPDRmf/ajblMYDOEIoa61t2YUbmqm7Uq"
    "k3FU0DTyEmiO2GYIQYDOAt7YQiawWYaijtg0kRTG5hO5ppsoMJ5SgLtpIM5Rh6IMQYBlC1Sg"
    "FqymBaWAWoEJK5BAc8Q2K/+pAmUoGkgtXP/NAuESG4jtGUitwN21KODb3InvEeER0kUb6HU2"
    "0ECCFK1VFegs8ycWTO0N5iA+wnfB2NyUXNGNLwr4S20gQeyUar50AQLcTQTt+WNN2m4+EZ0W"
    "eR71eBpJdqG38TEuYBJ/IMnZwMgKNlDDvFnwnELIXs/x+awM3PpV7KLSirzXmdtRwnlM4HfU"
    "oQjdAqs6fNHlIzC+TvFZFZGDUujDp3g/56+4K4U8+CggQKFdCisP7H6pwKZkXh33/ANTsU1l"
    "btjzJAAAAABJRU5ErkJggg==")
