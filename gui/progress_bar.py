# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
from wx.lib.pubsub import pub

###########################################################################
## Class MyDialog1
###########################################################################


class LoadingDialog(wx.Dialog):
    def __init__(self, range_size, title, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=title, pos=wx.DefaultPosition,
                           size=wx.Size(677, 73), style=wx.STAY_ON_TOP | wx.DEFAULT_DIALOG_STYLE)
        # Set window-size:
        self.SetSizeHintsSz(wx.Size(677, 73), wx.Size(677, 73))
        # Main-Sizer:
        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        # Widgets in Main-Sizer:
        self.static_text = wx.StaticText(self, wx.ID_ANY, "Loading...", wx.DefaultPosition, wx.DefaultSize,
                                         wx.ALIGN_CENTRE)
        self.gauge = wx.Gauge(self, wx.ID_ANY, range_size, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL)
        self.gauge.SetValue(0)
        # Add Widgets to Main-Sizer:
        self.main_sizer.Add(self.static_text, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 1)
        self.main_sizer.Add(self.gauge, 1, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 3)
        # Set Main-Sizer to Dialog:
        self.SetSizer(self.main_sizer)
        self.Layout()
        self.Centre(wx.BOTH)

        # Binds and Subscribes:
        pub.subscribe(self.update_status, "update_status")
        pub.subscribe(self.update_gauge_value, "update_gauge_value")

    def update_gauge_value(self, value):
        self.gauge.SetValue(value)
        self.main_sizer.Layout()

    def update_status(self, status):
        self.static_text.SetLabel(status)
        self.main_sizer.Layout()


if __name__ == "__main__":
    """
        Example of usage through a Thread.
    """
    import threading

    class TestThread(threading.Thread):
        """Test Thread Class."""
        def __init__(self):
            """Init Thread Class."""
            threading.Thread.__init__(self)

        def run(self):
            """Run Worker Thread."""
            # This is the code executing in the new thread.
            for i in xrange(1, 1000000):
                if i % 100000 == 0:
                    wx.CallAfter(pub.sendMessage, "update_status", status="Status:" + str(i))
                wx.CallAfter(pub.sendMessage, "update_gauge_value", value=i)
    app = wx.App()
    frame = wx.Frame(None)

    dialog = LoadingDialog(range_size=1000000, parent=frame)
    wx.CallAfter(TestThread().start)
    dialog.ShowModal()
    dialog.Destroy()

    app.MainLoop()