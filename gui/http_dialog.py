# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
from wx.lib.pubsub import pub
from ObjectListView import ObjectListView, ColumnDefn


def single_listener_subscribe(listener, topicName):
    if topicName in pub.topicsMap.keys():
        pub.unsubAll(topicName)
    pub.subscribe(listener, topicName)


def set_columns(olv, columns):
    """
        <List> columns = [ (attribute_name, column_name, column_width, column_image, str_convert), ...]
        column_image = -1   # No Image
    """
    olv_columns = list()
    for attribute_name, column_name, column_width, column_image, str_convert in columns:
        olv_columns.append(ColumnDefn(column_name, "left", column_width, attribute_name, imageGetter=column_image, stringConverter=str_convert))
    olv.SetColumns(olv_columns)


###########################################################################
## Class MyDialog1
###########################################################################
class HttpDialog(wx.Dialog):
    def __init__(self, parent, title="HTTP Statistics"):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=title, pos=wx.DefaultPosition, size=wx.Size(450, 350),
                           style=wx.STAY_ON_TOP | wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        # Set window-size:
        self.SetSizeHintsSz(wx.Size(450, 350), wx.DefaultSize)
        # Main-Sizer:
        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        # Widgets in Main-Sizer:
        self.static_text = wx.StaticText(self, wx.ID_ANY, title, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE)
        self.sites_list = ObjectListView(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         wx.LC_AUTOARRANGE | wx.LC_HRULES | wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_VRULES)
        # Add Widgets to Main-Sizer:
        self.main_sizer.Add(self.static_text, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 1)
        self.main_sizer.Add(self.sites_list, 1, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 3)
        # Set Main-Sizer to Dialog:
        self.SetSizer(self.main_sizer)
        self.Layout()
        self.Centre(wx.BOTH)

        # Binds and Subscribes:
        single_listener_subscribe(self.set_columns, "set_http_columns")
        single_listener_subscribe(self.set_sites, "set_http_sites")

    def set_columns(self, columns):
        """
            <List> columns = [ (attribute_name, column_name, column_width, column_image, str_convert), ...]
            column_image = -1   # No Image
        """
        set_columns(self.sites_list, columns)

    def set_sites(self, objs):
        self.sites_list.SetObjects(objs)

if __name__ == "__main__":
    """
        Example of usage.
    """
    app = wx.App()
    frame = wx.Frame(None)
    dialog = HttpDialog(parent=frame)
    dialog.ShowModal()
    dialog.Destroy()
    app.MainLoop()