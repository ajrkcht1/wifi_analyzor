# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
## https://sourceforge.net/projects/objectlistview/files/objectlistview-python/v1.2/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################
from wx.lib.pubsub import pub
from auiNotebook import AuiNotebook
from gui import images
import wx
import wx.xrc
import wx.aui
import wx.richtext

###########################################################################
## Class MyFrame
###########################################################################


class Frame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=title, pos=wx.DefaultPosition,
                          size=wx.Size(986, 717), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetIcon(images.Computer32.getIcon())
        # Widgets:
        self.aui_notebook = None
        self.status_bar = None
            # Menu:
        self.menu_bar = None
        self.menu_database = None
        self.menu_help = None
        self.menu_item_exit = None
        self.menu_item_kibana = None
        self.menu_item_kibana_dashboard = None
        self.menu_item_about = None
        self.menu_item_refresh = None

        # Build the windows
        self.menu_file = wx.Menu()
        self.set_window_size()
        self.set_menu_bar()
        self.set_frame()

        self.controller_handler = self.aui_notebook.splitter

    def set_window_size(self):
        """
            Set minimum size and maximum size of windows.
        """
        self.SetSizeHintsSz(wx.Size(850, 625), wx.DefaultSize)

    def set_menu_bar(self):
        self.menu_bar = wx.MenuBar(0)

        self.menu_file = wx.Menu()
        self.menu_database = wx.Menu()
        self.menu_help = wx.Menu()

        self.menu_item_kibana = wx.MenuItem(self.menu_database, wx.ID_ANY, "Open Kibana Database", wx.EmptyString, wx.ITEM_NORMAL)
        self.menu_item_kibana_dashboard = wx.MenuItem(self.menu_database, wx.ID_ANY, "Open Kibana DashBoard", wx.EmptyString, wx.ITEM_NORMAL)
        self.menu_item_about = wx.MenuItem(self.menu_help, wx.ID_ANY, "About...", wx.EmptyString, wx.ITEM_NORMAL)
        self.menu_item_refresh = wx.MenuItem(self.menu_help, wx.ID_ANY, "Rescan and Refresh All", wx.EmptyString, wx.ITEM_NORMAL)
        self.menu_item_exit = wx.MenuItem(self.menu_file, wx.ID_ANY, "Exit", wx.EmptyString, wx.ITEM_NORMAL)

        self.menu_file.AppendItem(self.menu_item_refresh)
        self.menu_file.AppendItem(self.menu_item_exit)
        self.menu_database.AppendItem(self.menu_item_kibana)
        self.menu_database.AppendItem(self.menu_item_kibana_dashboard)
        self.menu_help.AppendItem(self.menu_item_about)

        self.menu_bar.Append(self.menu_file, "File")
        self.menu_bar.Append(self.menu_database, "Database")
        self.menu_bar.Append(self.menu_help, "Help")

        self.SetMenuBar(self.menu_bar)

        self.Bind(wx.EVT_MENU, self.on_exit_item, self.menu_item_exit)
        self.Bind(wx.EVT_MENU, self.on_kibana_item, self.menu_item_kibana)
        self.Bind(wx.EVT_MENU, self.on_kibana_dashboard_item, self.menu_item_kibana_dashboard)
        self.Bind(wx.EVT_MENU, self.on_about_item, self.menu_item_about)
        self.Bind(wx.EVT_MENU, self.on_refresh_item, self.menu_item_refresh)

    def set_frame(self):
        frame_sizer = wx.BoxSizer(wx.VERTICAL)
        self.aui_notebook = AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_DEFAULT_STYLE)
        frame_sizer.Add(self.aui_notebook, 1, wx.ALL | wx.EXPAND, 1)

        self.SetSizer(frame_sizer)
        self.Layout()
        self.status_bar = self.CreateStatusBar(1, wx.ST_SIZEGRIP, wx.ID_ANY)

        self.Centre(wx.BOTH)

    def add_page(self, name):
        controller_handle = self.aui_notebook.add_profile(name, self.aui_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        return controller_handle

    # ----------------------Bind-Functions----------------------

    def on_exit_item(self, event):
        pub.sendMessage("exit", event=event)

    def on_kibana_item(self, event):
        pub.sendMessage("kibana", event=event)

    def on_kibana_dashboard_item(self, event):
        pub.sendMessage("kibana_dashboard", event=event)

    def on_about_item(self, event):
        pub.sendMessage("about", event=event)

    def on_refresh_item(self, event):
        pub.sendMessage("rescan_and_refresh", event=event)