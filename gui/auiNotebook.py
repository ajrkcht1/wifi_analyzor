import wx
import wx.aui
import wx.richtext
from splitter import Splitter


class AuiNotebook(wx.aui.AuiNotebook):
    def __init__(self, *args, **kwargs):
        wx.aui.AuiNotebook.__init__(self, *args, **kwargs)

        # Main panel:
        self.main_panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            # Widgets in main panel:
        sizer_above_panel = wx.BoxSizer(wx.VERTICAL)
        self.splitter = Splitter.build_main_splitter(self.main_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D)
        sizer_above_panel.Add(self.splitter, 1, wx.EXPAND, 1)
            # Set widgets in main panel:
        self.main_panel.SetSizer(sizer_above_panel)
        self.main_panel.Layout()
        sizer_above_panel.Fit(self.main_panel)

        self.AddPage(self.main_panel, "Main", True)

    @staticmethod
    def add_profile(name, parent, *args, **kwargs):
        panel = wx.Panel(parent, *args, **kwargs)
        aui_notebook = parent
        sizer_above_panel = wx.BoxSizer(wx.VERTICAL)

        splitter = Splitter.build_profile_splitter(panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D)
        sizer_above_panel.Add(splitter, 1, wx.EXPAND, 1)

        panel.SetSizer(sizer_above_panel)
        panel.Layout()
        sizer_above_panel.Fit(panel)
        aui_notebook.AddPage(panel, "Profile - {0}".format(name), False)
        return splitter
