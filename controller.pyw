import json
import webbrowser
import threading
import time
import os
import wx
from functools import partial
from wx.lib.pubsub import pub
from wx.lib.wordwrap import wordwrap
from gui import images
from gui.progress_bar import LoadingDialog
from gui.http_dialog import HttpDialog
from gui.view import Frame
from model.network import Network


def initiate_window(controller):
    computer_image = controller.controller_handler.add_image_to_computers(images.Computer16, images.Computer32)
    linux_image = controller.controller_handler.add_image_to_computers(images.Linux16, images.Linux32)
    microsoft_image = controller.controller_handler.add_image_to_computers(images.Microsoft16, images.Microsoft32)
    apple_image = controller.controller_handler.add_image_to_computers(images.Apple16, images.Apple32)

    def os_image_getter(computer):
        for os in computer.operating_system:
            if "linux" in os.lower():
                return linux_image
            elif "microsoft" in os.lower():
                return microsoft_image
            elif "apple" in os.lower() or "mac" in os.lower():
                return apple_image
        return -1
    columns = [
        ("ip", "IP", 130, computer_image, None),
        ("mac", "Mac", 140, -1, None),
        ("status", "Status", 50, -1, None),
        ("vendor", "Vendor", 120, -1, None),
        ("name", "Name", 80, -1, None),
        ("operating_system", "Operating System", 160, os_image_getter, lambda op: ', '.join(op)),
        ("ports", "Ports", 200, -1, lambda p: ', '.join([str(port) for port in p.opened_ports]))
    ]
    controller.controller_handler.set_computers_columns(columns)
    while controller.network.computers is None:
        time.sleep(1)
    controller.controller_handler.add_computers(controller.network.computers.values())
    controller.controller_handler.set_computers_menu({"Open Profile": partial(add_page, controller),
                                                      "Scan Opened Ports": lambda c: threading.Thread(target=controller.network.scan, args=(c, controller.network, "")).start(),
                                                      "Save In Database": lambda c: threading.Thread(target=controller.network.save_in_database, args=(c, controller.network, "",)).start(),
                                                      "Open In Database": lambda c: threading.Thread(target=open_in_database, args=(c, controller,)).start(),
                                                      "Open Database DashBoard": lambda c: open_dashboard(controller, c)
                                                      })


def open_dashboard(controller, computer):
    new_index = "scan-{ip}".format(ip=computer.ip)
    if new_index in controller.network.database.handle.indices.get_aliases().keys():
        controller.network.database.handle.index(index='.kibana', doc_type="index-pattern", id=new_index, body={"title": new_index, "timeFieldName": "starttime", "fields": "[{\"name\":\"country\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"_index\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":false,\"analyzed\":false,\"doc_values\":false},{\"name\":\"starttime\",\"type\":\"date\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":false,\"doc_values\":true},{\"name\":\"type\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"mac\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"elapsed\",\"type\":\"number\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":false,\"doc_values\":true},{\"name\":\"protocol\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"ipv4\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"vendor\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"ipv6\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"state\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"product\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"address\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"endtime\",\"type\":\"date\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":false,\"doc_values\":true},{\"name\":\"service-data\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"port\",\"type\":\"number\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":false,\"doc_values\":true},{\"name\":\"service\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"_source\",\"type\":\"_source\",\"count\":0,\"scripted\":false,\"indexed\":false,\"analyzed\":false,\"doc_values\":false},{\"name\":\"status\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":true,\"analyzed\":true,\"doc_values\":false},{\"name\":\"_id\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":false,\"analyzed\":false,\"doc_values\":false},{\"name\":\"_type\",\"type\":\"string\",\"count\":0,\"scripted\":false,\"indexed\":false,\"analyzed\":false,\"doc_values\":false},{\"name\":\"_score\",\"type\":\"number\",\"count\":0,\"scripted\":false,\"indexed\":false,\"analyzed\":false,\"doc_values\":false}]"})
        controller.change_visualisation(doc_type="search", id="scan_search", new_index=new_index)
        controller.change_visualisation(doc_type="visualization", id="port-precent", new_index=new_index)
        controller.change_visualisation(doc_type="visualization", id="port_count_elapsed_time_chart", new_index=new_index)
        url = r"http://localhost:{kibana_port}/app/kibana#/dashboard/scan_dashboard".format(kibana_port="5601")
        webbrowser.open(url)
    else:
        pub.sendMessage("warn", message="Error: This computer doesn't have DashBoard since it hasn't been scanned yet.", caption="Error")


def open_in_database(computer, controller):
    url = r"http://localhost:{kibana_port}/app/kibana#/doc/computers/computers/computer/{ip}".format(kibana_port="5601", ip=computer.ip)
    webbrowser.open(url)


def add_page(controller, computer):
    # Build Information:
    page_controller_handle = controller.view.add_page(computer.ip)
    computer_image = page_controller_handle.add_image_to_information(images.Computer16, images.Computer32)
    linux_image = page_controller_handle.add_image_to_information(images.Linux16, images.Linux32)
    microsoft_image = page_controller_handle.add_image_to_information(images.Microsoft16, images.Microsoft32)
    apple_image = page_controller_handle.add_image_to_information(images.Apple16, images.Apple32)

    def os_image_getter(computer):
        for os in computer.operating_system:
            if "linux" in os.lower():
                return linux_image
            elif "microsoft" in os.lower():
                return microsoft_image
            elif "apple" in os.lower() or "mac" in os.lower():
                return apple_image
        return -1
    columns = [
        ("ip", "IP", 130, computer_image, None),
        ("mac", "Mac", 140, -1, None),
        ("status", "Status", 50, -1, None),
        ("vendor", "Vendor", 120, -1, None),
        ("name", "Name", 80, -1, None),
        ("operating_system", "Operating System", 160, os_image_getter, lambda op: ', '.join(op)),
        ("ports", "Ports", 200, -1, lambda p: ', '.join([str(port) for port in p.opened_ports]))
    ]
    page_controller_handle.set_information_columns(columns)
    page_controller_handle.add_information([computer])
    # Build Processes:
    columns = [
        ("Caption", "Name", -1, -1, None),
        ("ProcessId", "PID", -1, -1, None),
        ("ProcessId", "CPU", -1, -1, lambda pid: "{0:.2f}%".format(computer.information.processes_cpu[pid])),
        ("CSName", "User-Name", 100, -1, None),
        ("ThreadCount", "Threads", 60, -1, None),
        ("Description", "Description", -1, -1, None),
        ("CommandLine", "CommandLine", 160, -1, None)
    ]
    page_controller_handle.set_processes_columns(columns)
    page_controller_handle.set_processes_menu({"Kill": lambda p: computer.information.kill_process(p.ProcessId)})
    # Build Services:
    columns = [
        ("Name", "Name", 130, -1, None),
        ("ProcessId", "PID", -1, -1, None),
        ("Status", "Status", 50, -1, None),
        ("State", "State", 70, -1, None),
        ("StartMode", "Start-Mode", 80, -1, None),
        ("ServiceType", "Type", -1, -1, None),
        ("StartName", "Started by", 90, -1, None),
        ("PathName", "Path", 160, -1, None),
        ("Description", "Description", 200, -1, None)
    ]
    page_controller_handle.set_services_columns(columns)
    page_controller_handle.set_services_menu({"Stop": lambda s: computer.information.stop_service(s.Name),
                                              "Start": lambda s: computer.information.start_service(s.Name),
                                              "Refresh": lambda s: computer.information.refresh_service(s.Name)})
    single_listener_subscribe(computer.open_in_database, "database_button")
    if "known_computer.KnownComputer" in str(type(computer)):
        computer.open_http_dialog = lambda: controller.open_http_dialog(computer)
        single_listener_subscribe(computer.open_http_dialog, "http_button")
        single_listener_subscribe(computer.information.refresh, "refresh_button")
        single_listener_subscribe(page_controller_handle.add_services, "set_services")
        single_listener_subscribe(page_controller_handle.add_processes, "set_processes")
        single_listener_subscribe(page_controller_handle.refresh_services_list, "refresh_services_list")
        single_listener_subscribe(page_controller_handle.refresh_processes_list, "refresh_processes_list")
        threading.Thread(target=computer.information.start_monitoring).start()


def single_listener_subscribe(listener, topicName):
    if topicName in pub.topicsMap.keys():
        pub.unsubAll(topicName)
    pub.subscribe(listener, topicName)


class Controller(wx.App):
    def __init__(self, redirect=False, filename=None, useBestVisual=False, clearSigInt=True):
        wx.App.__init__(self, redirect, filename, useBestVisual, clearSigInt)

        pub.subscribe(self.warn, "warn")
        pub.subscribe(self.rescan_and_refresh, "rescan_and_refresh")
        pub.subscribe(self.open_kibana, "kibana")
        pub.subscribe(self.open_kibana_dashboard, "kibana_dashboard")
        pub.subscribe(self.open_about, "about")
        pub.subscribe(self.on_exit, "exit")

        pub.subscribe(self.initiate_window, "initiate_window")
        pub.subscribe(self.start_progress_bar, "start_progress_bar")
        pub.subscribe(self.destroy_progress_bar, "destroy_progress_bar")
        pub.subscribe(self.write_shell, "write_shell")

        self.view = Frame(None, title="Wifi Analyzer")
        self.view.Show()
        self.controller_handler = self.view.controller_handler
        self.network = Network(self.controller_handler.get_shell_stdout())
        self.start_network()
        self.dialog = LoadingDialog(range_size=10000, title="Nmap Scanning", parent=self.view)

        pub.subscribe(self.controller_handler.refresh_computers_list, "refresh_all_computers_list")
        pub.subscribe(self.controller_handler.refresh_computer, "refresh_computer_in_list")
        pub.subscribe(self.controller_handler.add_computers, "set_computers_list")

    def start_network(self):
        thread = threading.Thread(target=self.network.start_network, args=(self,))
        wx.CallAfter(thread.start)

    def initiate_window(self):
        thread = threading.Thread(target=initiate_window, args=(self,))
        wx.CallAfter(thread.start)

    def start_progress_bar(self, title):
        if self.dialog is None:
            self.dialog = LoadingDialog(range_size=10000, title=title, parent=self.view)
        self.dialog.ShowModal()

    def destroy_progress_bar(self):
        self.dialog.Destroy()
        self.dialog = None

    def write_shell(self, output):
        self.controller_handler.get_shell_stdout()(output)

    def rescan_and_refresh(self, event):
        wx.CallAfter(threading.Thread(target=self.network.refresh).start)

    def open_kibana(self, event):
        url = "http://localhost:{kibana_port}/app/kibana#/discover/computers_search".format(kibana_port="5601")
        webbrowser.open(url)

    def open_kibana_dashboard(self, event):
        url = "http://localhost:{kibana_port}/app/kibana#/dashboard/computers_dashboard".format(kibana_port="5601")
        webbrowser.open(url)

    def open_about(self, event):
        info = wx.AboutDialogInfo()
        info.SetName("About Wifi Analyzer")
        info.SetVersion("Version 1.0")
        info.SetCopyright("(C) Copyright 2016. All rights reserved, by Shachar Lavi.")
        description = """
"Wifi Analyzer v1.0" allows an IT professional to supervise the individual components of a network within a larger network management framework.

It allows:
  * Network Device Discovery - Identifying what devices are present on a network.
  * Network Device Scanning - Scanning devices to determined useful information (Opened Ports, OS, Mac, Vendor).
  * Network Device Monitoring - Monitoring the device activity (Processes, Services and HTTP packets flow).
  * Network Device Controlling - Controlling the device activity (Processes and Services).
  * Network Database Visualisation - A graphic user interface for database visualisation (Graphs, Cakes).

"Wifi Analyzer v1.0" was developed during high school lessons as a part of a "Bagrut Project" by a 12th grade student.

Enjoy!!
"""
        info.SetDescription(wordwrap(description, 630, wx.ClientDC(self.view)))
        info.SetIcon(images.Computer32.GetIcon())
        # Show the wx.AboutBox
        wx.AboutBox(info)

    def on_exit(self, event):
        self.view.Destroy()

    def warn(self, message, caption='Warning!'):
        dlg = wx.MessageDialog(self.view, message, caption, wx.OK | wx.ICON_WARNING)
        dlg.ShowModal()
        dlg.Destroy()

    def open_http_dialog(self, computer):
        dialog = HttpDialog(parent=self.view)
        columns = [
            ("url", "URL", 180, -1, None),
            ("host", "Host", 120, -1, None),
            ("browser", "Browser", 50, -1, None),
            ("language", "Language", 50, -1, None),
            ("encoding", "Encoding", 50, -1, None),
            ("times", "Times", 50, -1, None),
        ]
        dialog.set_columns(columns)
        dialog.set_sites(objs=computer.http_statistics.sites)
        dialog.ShowModal()
        dialog.Destroy()

    def change_visualisation(self, doc_type, id, new_index):
        source = self.network.database.handle.get(index='.kibana', doc_type=doc_type, id=id)["_source"]
        json_str = json.loads(source["kibanaSavedObjectMeta"]["searchSourceJSON"])
        json_str["index"] = new_index
        source["kibanaSavedObjectMeta"]["searchSourceJSON"] = json.dumps(json_str)
        self.network.database.handle.index(index='.kibana', doc_type=doc_type, id=id, body=source)

if __name__ == "__main__":
    controller = Controller()
    controller.MainLoop()
    os._exit(0)