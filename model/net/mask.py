import socket
import subprocess


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_mask():
    ip = get_ip_address()
    output = subprocess.check_output("ipconfig", shell=True)
    results = list(dict())
    for row in output.split('\n'):
        if ': ' in row:
            key, value = row.split(': ')
            key = key.strip(' .')
            value = value.strip()
            for result in results:
                if key not in result.keys():
                    result[key] = value
                    break
            else:
                results.append(dict())
    for result in results:
        if "IPv4 Address" in result.keys() and result["IPv4 Address"] == ip:
            return result["Subnet Mask"]
    raise Exception("Didn't find result on the command 'ipconfig'")


def get_mask_size():
    mask = get_mask()
    size = sum([bin(int(i)).count('1') for i in mask.split('.')])
    return size


def get_lan(minimum_size=20):
    size = get_mask_size()
    if size < minimum_size:
        size = minimum_size
    lan = "{ip}/{size}".format(ip=get_ip_address(), size=size)
    return lan