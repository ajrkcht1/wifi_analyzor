import logging
import sys


#------------Start of Class <Log>------------
class Log(object):
    # -------Constant Variables-------
    CRITICAL = logging.CRITICAL
    DEBUG    = logging.DEBUG
    INFO     = logging.INFO
    #---------------------------------

    def __init__(self, filename="log.log", logging_level=logging.INFO, is_file=False, class_name=__name__):
        self.filename      = filename
        self.class_name    = class_name
        self.logging_level = logging_level
        self.log           = self.__create(is_file)

    def __create(self, is_file):
        """
            Create the appropriate handle with the appropriate level
        """
        log = logging.getLogger(self.class_name)
        if is_file:
            file_handler = logging.FileHandler(filename=self.filename, mode='a')
            log.addHandler(create_handle(file_handler, Log.DEBUG))
        stream_handle = logging.StreamHandler(sys.stdout)
        log.addHandler(create_handle(stream_handle, self.logging_level))
        log.setLevel(self.logging_level)
        return log
#------------End of Class <Log>------------


def create_handle(handle, logging_level):
    """
       Create an handle
    """
    handle.setLevel(logging_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handle.setFormatter(formatter)
    return handle


#------------Main------------
def main():
    """
        Example of usage
    """
    log = Log(class_name=__name__).log
    x   = 5
    log.debug("x = 5")
    raw_input("Press <Enter> to Exit...")

if __name__ == "__main__":
    main()
#------------End of Code------------