import threading


def thread_safe_function(func):
    """
        Decorator which makes sure that the decorated function is thread safe.
    """
    lock = threading.Lock()

    def run_thread_safe(*args, **kwargs):
        lock.acquire()
        try:
            result = func(*args, **kwargs)
        except Exception, error:
            raise error
        finally:
            lock.release()
        return result

    return run_thread_safe