class Attribute(object):
    """
        This object simulates self.
        You create the object with given self.
        And then you can use this object to get any attributes you want.
        Doing that without calling:
            original_self.__getattr__
            original_self.__getattribute__
            original_self.__setattr__
    """

    def __init__(self, original_self):
        self.original_self = original_self

    def __delattr__(self, name):
        """ x.__delattr__('name') <==> del x.name """
        return object.__delattr__(self.original_self, name)

    def __format__(self, *args, **kwargs):
        """ default object formatter """
        return object.__format__(self.original_self, *args, **kwargs)

    def __getattribute__(self, item):
        if item == "original_self":
            return object.__getattribute__(self, item)
        """ x.__getattribute__('name') <==> x.name """
        return object.__getattribute__(self.original_self, item)

    def __hash__(self):
        """ x.__hash__() <==> hash(x) """
        return object.__hash__(self.original_self)

    def __reduce_ex__(self, *args, **kwargs):
        """ helper for pickle """
        return object.__reduce_ex__(self.original_self, *args, **kwargs)

    def __reduce__(self, *args, **kwargs):
        """ helper for pickle """
        return object.__reduce__(self.original_self, *args, **kwargs)

    def __repr__(self):
        """ x.__repr__() <==> repr(x) """
        return object.__repr__(self.original_self)

    def __setattr__(self, name, value):
        """ x.__setattr__('name', value) <==> x.name = value """
        if name == "original_self":
            return object.__setattr__(self, name, value)
        return object.__setattr__(self.original_self, name, value)

    def __sizeof__(self):
        """
        __sizeof__() -> int
        size of object in memory, in bytes
        """
        return object.__sizeof__(self.original_self)

    def __str__(self):
        """ x.__str__() <==> str(x) """
        return object.__str__(self.original_self)


def inside_func(func):
    """
        Decorator that make the function get attributes without calling:
            __get_attr__
            __get_attribute__
        It calls directly to object.__getattribute__ when you use self.
        YOU STILL USE 'self' TO GET ATTRIBUTE.
    """
    def new_func(*args, **kwargs):
        original_self = args[0]
        new_self = Attribute(original_self)
        new_args = tuple([new_self] + list(args)[1:])
        result = func(*new_args, **kwargs)
        return result
    return new_func