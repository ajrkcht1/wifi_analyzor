import os
import win32security
from model.process_control.privilege import Privilege


#------------Start of Class <Privileges>------------
class Privileges(object):
    def __init__(self, process):
        """
        Parameters:
            Process process
        """
        self.process = process
        self.privileges = self.__get_privileges()

    def __get_privileges(self):
        """
            Return list of all privileges of self.process .
            Get: None
            Return type: List<Privilege>
        """
        privileges = [Privilege(priv_name, priv_flag) for priv_name, priv_flag in self.process.token_information]
        return privileges

    def add_privilege(self, new_privilege_name):
        """
            Get a new_privilege_name and add/toggle it (update self.token_information and self.privileges)
            Get: String new_privilege_name
            Return type: None
        """
        privilege_id = win32security.LookupPrivilegeValue("", new_privilege_name)
        privileges_names = [privilege.name for privilege in self.privileges]
        if new_privilege_name in privileges_names:
            self.__toggle_privilege(privilege_id)
        else:
            self.__append_privilege(privilege_id)

    def __toggle_privilege(self, param_name_id):
        """
            Get an ID of privilege and toggle its flag.
            Get: Int param_name_id
            Return type: None
        """
        new_token_information = []
        for privilege in self.privileges:
            if privilege.name_id == param_name_id:
                    privilege.flag_id ^= 0b10
            new_token_information.append((privilege.name_id, privilege.flag_id))
        self.__update_all_privileges(tuple(new_token_information))

    def __update_all_privileges(self, new_token_information):
        """
            Update/Change to the new privileges and
            update the necessary attributes (self.process.token_information and self.privileges)
            Get: List<tuple<int>> new_token_information
            Return type: None
        """
        win32security.AdjustTokenPrivileges(self.process.token_handler, 0, new_token_information)
        self.process.token_information = win32security.GetTokenInformation(self.process.token_handler, win32security.TokenPrivileges)
        self.privileges = self.__get_privileges()   # Update the self.privileges value

    def __append_privilege(self, privilege_id):
        """
            Add a new privilege that not exists.
            TODO Add new privilege to process (There is no such thing in python, so the function doesn't work)
            Get: Int privilege_id
            Return type: None
        """
        new_token_information = list(self.process.token_information)
        new_token_information.append((privilege_id, win32security.SE_PRIVILEGE_ENABLED))
        self.__update_all_privileges(tuple(new_token_information))

    def __str__(self):
        """ x.__str__() <==> str(x) """
        return self.to_string()

    def to_string(self, separator=False):
        """
            Return a string of self.process's privileges.
            if separator: return with separator.
            Get: [ Boolean separator ]
            Return type: String
        """
        output = ""
        if separator:
            output += '-'*80 + os.linesep
        for privilege in self.privileges:
            output += "{name}: {flag}".format(name=privilege.name, flag=privilege.flag) + os.linesep
        return output

#------------End of Class <Privileges>------------
#------------End of Code------------