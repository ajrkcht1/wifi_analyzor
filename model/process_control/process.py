import os
import win32security
import win32api
import psutil
import win32con
from model.process_control.privileges import Privileges


#------------Start of Class <Process>------------
class Process(object):
    # -------Constant Variables-------
    ERROR_NO_SUCH_PROCESS = "(87, 'OpenProcess'"
    ERROR_ACCESS_DENIED = "(5, 'OpenProcess'"
    #---------------------------------

    def __init__(self, pid):
        """
        Parameters:
            Int pid
        """
        # Win32Api-Variables:
        self.access, self.is_such, self.process_handler, self.token_handler, self.token_information = self.__get_win32api_variables(pid)
        # Psutil-Variables:
        self.psutil_handler = psutil.Process(pid)
        # Info-Variables:
        dic_info = self.psutil_handler.as_dict(attrs=["pid", "status", "memory_percent", "cpu_percent",
                                                      "create_time", "exe", "name", "num_handles",
                                                      "num_threads", "username"], ad_value="AccessDenied")
            # Constant-Info-Variables:
        self.pid            = dic_info["pid"]
        self.name           = dic_info["name"]
        self.username       = dic_info["username"]
        self.exe            = dic_info["exe"]
        self.create_time    = dic_info["create_time"]

            # Change-Info-Variables:    (Change during time, every variable has get-function (in psutil))
        self.status         = dic_info["status"]
        self.memory_percent = dic_info["memory_percent"]
        self.cpu_percent    = dic_info["cpu_percent"]
        self.num_handles    = dic_info["num_handles"]
        self.num_threads    = dic_info["num_threads"]

        self.privileges     = Privileges(self)   # Must be last attribute (using Process's attributes)

    def __get_win32api_variables(self, pid):
        """
            Return all Win32Api-Variables:
                Boolean access, Boolean is_such, win32object process_handler,
                win32object token_handler, List<Tuple<int>> token_information
            Get: int pid
            Return type: Tuple
        """
        access = False
        is_such = False
        process_handler = None
        token_handler = None
        token_information = []
        try:
            process_handler = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION, False, pid)
            token_handler = win32security.OpenProcessToken(process_handler, win32security.TOKEN_ALL_ACCESS)
            token_information = win32security.GetTokenInformation(token_handler, win32security.TokenPrivileges)
            access  = True
            is_such = True
        except Exception, e:
            if Process.ERROR_ACCESS_DENIED in str(e):
                is_such = True
            elif Process.ERROR_NO_SUCH_PROCESS in str(e):
                access = True
            else:
                raise e
        return access, is_such, process_handler, token_handler, token_information

    # -------ToString-Functions-------
    def privileges_to_string(self, separator=False):
        """
            Return a string of self.process's privileges.
            if separator: return with separator.
            Get: [ Boolean separator ]
            Return type: String
        """
        return self.privileges.to_string(separator)

    def to_string(self):
        """
            Return a string of self.process's attributes:
                Int pid
                String username
                String exe
                String create_time
                String status
                Float memory_percent
                Float cpu_percent
                Int num_handles
                Int num_threads
            Get: None
            Return type: String
        """
        output = "\t###->> {name} <<-###".format(name=self.name) + os.linesep
        output += "pid: {pid}{sep}username: {username}{sep}exe: {exe}{sep}".format(pid=self.pid,username=self.username.encode('utf-8'), exe=self.exe, sep=os.linesep)
        output += "create_time: {create_time}{sep}status: {status}{sep}".format(create_time=self.create_time, status=self.status, sep=os.linesep)
        output += "memory_percent: {memory_percent}{sep}cpu_percent: {cpu_percent}{sep}".format(memory_percent=self.memory_percent, cpu_percent=self.cpu_percent, sep=os.linesep)
        output += "num_handles: {num_handles}{sep}num_threads: {num_threads}{sep}".format(num_handles=self.num_handles, num_threads=self.num_threads, sep=os.linesep)
        output += "Privileges:{sep}".format(sep=os.linesep)
        output += self.privileges_to_string() + os.linesep
        output += "-"*80
        return output

    def __str__(self):
        return self.to_string()

    #  -------Command-Functions-------
    def add_privilege(self, new_privilege_name):
        """
            Add Privilege to a Process.
            Get: String new_privilege_name
            Return type: None
        """
        self.privileges.add_privilege(new_privilege_name)

    def suspend(self):
        """
            Suspend this process.
            Get: None
            Return type: None
        """
        self.psutil_handler.suspend()

    def resume(self):
        """
            Resume this process (after it was suspend).
            Get: None
            Return type: None
        """
        self.psutil_handler.resume()

    def kill(self):
        """
            Kill this Process.
            Get: None
            Return type: None
        """
        process_handle_to_exit = win32api.OpenProcess(1, False, self.pid)
        win32api.TerminateProcess(process_handle_to_exit, -1)
        win32api.CloseHandle(process_handle_to_exit)
        win32api.CloseHandle(self.process_handler)
        win32api.CloseHandle(self.token_handler)

    def __getattr__(self, item):
        """
            If attribute is not in Process class so take it from psutil.Process class.
            Get: attribute
            Return type: func/value
        """
        return getattr(self.psutil_handler, item)
#------------End of Class <Process>------------
#------------End of Code------------
