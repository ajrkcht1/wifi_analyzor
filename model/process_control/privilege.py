import win32security


#------------Start of Class <Privilege>------------
class Privilege(object):
    # -------Constant Variables-------
    SE_PRIVILEGE_DISABLE = 0
    flag_to_string = {
        SE_PRIVILEGE_DISABLE: "Disabled (Disabled by Default)",
        win32security.SE_PRIVILEGE_ENABLED: "Enabled (Disabled by Default)",
        win32security.SE_PRIVILEGE_ENABLED_BY_DEFAULT: "Disabled (Enabled by Default)",
        win32security.SE_PRIVILEGE_REMOVED: "Removed",
        win32security.SE_PRIVILEGE_ENABLED_BY_DEFAULT | win32security.SE_PRIVILEGE_ENABLED: 'Enabled (Enabled by Default)',
        win32security.SE_PRIVILEGE_USED_FOR_ACCESS: "Used for Access"}
    #---------------------------------

    def __init__(self, name_id, flag_id):
        """
        Parameters:
            Int name_id, Int flag_id
        """
        self.name_id = name_id
        self.flag_id = flag_id
        self.name    = win32security.LookupPrivilegeName("", self.name_id)
        self.flag    = Privilege.flag_to_string[self.flag_id]
#------------End of Class <Privilege>------------
#------------End of Code------------