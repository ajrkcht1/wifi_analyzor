#!/usr/bin/python
# -*- coding: <utf-8> -*-
from model.information import Information
from model.computer import Computer
from model.http_statistics import HttpStatistics


#------------Start of Class <KnownComputer>------------
class KnownComputer(Computer):
    def __init__(self, computer, name):
        """
        Parameters:
            Computer computer or UnknownComputer computer, String name
        """
        Computer.from_computer(computer, self)
        self.name = name
        self.status = "up"
        self.information = Information(self)
        self.http_statistics = HttpStatistics(self)

    @property
    def __dict__(self):
        dic = {'status': self.status,
               'operating_system': self.operating_system,
               'vendor': self.vendor,
               'name': self.name,
               'ip': self.ip,
               'mac': self.mac,
               'ports': self.ports.__dict__}
        return dic
#------------End of Class <KnownComputer>------------
#------------End of Code------------