import inspect
import threading
import jsonpickle
from wx.lib.pubsub import pub
from model.server import Server
from model.computer import Computer
from model.database import Database
from model.known_computer import KnownComputer
from model.net.mask import get_lan, get_ip_address
from model.nmap_package.nmap_report import NmapReport
from model.decorators.inside_class_decorator import inside_func, Attribute
from model.decorators.thread_safe_decorator import thread_safe_function


# ------------Start of Class <Network>------------
class Network(object):
    HELP_STRING = """-----------------------------------------------------------------------------------------
Usage:
    <IP> <IP-COMMAND> [Parameters]
  or
    <COMMAND>

COMMANDS:
    help                        \t\t\t\t| You Should know this command.
    computers                   \t\t\t| Print list of all computers.
    refresh                     \t\t\t\t| Rescan and Refresh all computers.

IP-COMMANDS:
    to_string                   \t\t\t| Print Computer's information.
    [ATTRIBUTE]                 \t\t\t| Print the inputted ATTRIBUTE (eg. 10.0.0.1 ports)
                                \t\t\t\t| ATTRIBUTE: (ports, vendor, name, ip, mac, operating_system)
    scan                        \t\t\t\t| Scan this IP for opened ports.
    database                    \t\t\t| Save this computer in database.
    open_in_database            \t\t\t| Open this computer in the database.
    json                        \t\t\t\t| Print the computer information in a JSON-FORMAT.
    known <NAME>                \t\t\t| Change this computer into KnownComputer by its name.
    information <WMI-COMMAND>   \t| Run WMI-COMMAND in this KnownComputer

    WMI-COMMANDS:
        create_process <NAME>       \t| Create process by its NAME (eg. "cmd.exe").
        kill_process    <PID>       \t| Kill process by PID.
        reboot_computer             \t| Reboot remote computer.
        get_processes               \t\t| Print all WMI-object processes.
        get_services                \t\t| Print all WMI-object services.
-----------------------------------------------------------------------------------------"""
    DISCOVER_KNOWN_SERVER_PORT = 8888
    HTTP_SERVER_PORT = 8889

    @inside_func
    def __init__(self, stdout, stdin=None, database=Database(index="computers")):
        """
        Parameters:
            None
        """
        self.stdout = stdout
        self.stdin = stdin
        self.database = database
        self.computers = None

    @inside_func
    def __get_computers(self, lan=None, verbose=True):
        """
            Creates Computer instance for every computer in the lan.
            Get: None
            Return type: dict<ip: Computer>
        """
        if lan is None:
            lan = get_lan(minimum_size=23)
        computers = dict()

        for nmap_host in NmapReport.get_computers_in_lan(lan, database=self.database.handle, verbose=verbose):
            c = Computer(ip=nmap_host.ipv4, mac=nmap_host.mac, status=nmap_host.status, vendor=nmap_host.vendor)
            computers[c.ip] = c
        return computers

    @inside_func
    def __run_http_thread(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        def thread():
            server = Server("0.0.0.0", Network.HTTP_SERVER_PORT)
            answer_function = self.__add_packet
            server.start(answer_function)
        t = threading.Thread(target=thread)
        t.start()
        self.stdout("http_thread has been started")

    @inside_func
    def __add_packet(self, client_socket, data):
        packet = data
        ip, port = client_socket.getpeername()  # Maybe client_socket.getpeername()
        if ip == "127.0.0.1":
            ip = get_ip_address()
        computer = self.computers[ip]
        computer.http_statistics.add_packet(packet)

    @inside_func
    def __run_discover_known_computers_thread(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        def thread():
            server = Server("0.0.0.0", Network.DISCOVER_KNOWN_SERVER_PORT)
            answer_function = self.__change_to_known
            server.start(answer_function)
        t = threading.Thread(target=thread)
        t.start()
        self.stdout("discover_known_computers_thread has been started")

    @inside_func
    def __change_to_known(self, client_socket, name):
        ip, port = client_socket.getpeername()  # Maybe client_socket.getpeername()
        if ip == "127.0.0.1":
            ip = get_ip_address()
        computer = self.computers[ip]
        if "KnownComputer" not in str(type(computer)):
            known_computer = KnownComputer(computer, name)
            self.computers[known_computer.ip] = known_computer
            pub.sendMessage("set_computers_list", objs=self.computers.values())
            pub.sendMessage("refresh_all_computers_list")
            self.stdout("Computer<ip={0}> changed to KnownComputer".format(known_computer.ip))
        return "ACK"

    @inside_func
    def refresh_network(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        new_computers = self.__get_computers()
        for ip, computer in self.computers.iteritems():
            if ip not in new_computers.keys():
                computer.status = "down"    # TODO class-constants
            else:
                computer.status = "up"      # TODO class-constants
                new_computers.pop(ip)
        for ip, new_computer in new_computers.iteritems():
            self.computers[ip] = new_computer
        pub.sendMessage("set_computers_list", objs=self.computers.values())
        pub.sendMessage("refresh_all_computers_list")

    @thread_safe_function
    def __getattribute__(self, item):
        """
            *Explanation*
            Use Mutex, Only one thread can use this object.
            Get:
            Return type:
        """
        return object.__getattribute__(self, item)

    # --------------Shell-Functions--------------
    @inside_func
    def print_computers(self, args):
        for computer in self.computers.values():
            self.stdout('-' * 80)
            self.stdout(computer)
        self.stdout('-' * 80)

    @inside_func
    def refresh(self, args=None):
        self.stdout("Refreshing...")
        self.refresh_network()

    @inside_func
    def scan(self, computer, network, args):
        self.stdout("Started Port Scan on Computer<ip={0}>".format(computer.ip))
        computer.refresh_opened_ports(database=self.database.handle)
        pub.sendMessage("refresh_computer_in_list", obj=computer)

    @inside_func
    def save_in_database(self, computer, network, args):
        computer.save_in_database(self.database)

    @inside_func
    def print_json(self, computer, network, args):
        self.stdout(jsonpickle.encode(computer, unpicklable=False))

    @inside_func
    def run_information_command(self, computer, network, args):
        if len(args) < 3:
            self.stdout("No command given")
            self.stdout("[IP] information [command]")
            return None
        try:
            attr = getattr(computer.information, args[2])
        except AttributeError:
            self.stdout("No Such command")
            return None
        finally:
            if inspect.ismethod(attr):
                self.stdout(args[2])
                if args[2] == "create_process" or args[2] == "kill_process":
                    if len(args) < 4:
                        self.stdout("No parameter given")
                        self.stdout("[IP] information [command] [parameter]")
                        return None
                    self.stdout(args[3])
                    output = attr(args[3])
                else:
                    output = attr()
                if output is not None:
                    self.stdout(output)
            else:
                self.stdout(attr)

    @inside_func
    def change_to_known(self, computer, network, args):
        if len(args) < 3:
            self.stdout("No Name given")
            self.stdout("[IP] known [name]")
            return None
        name = args[2]
        known_computer = KnownComputer(computer, name)
        network.computers[known_computer.ip] = known_computer
        pub.sendMessage("set_computers_list", objs=network.computers.values())
        pub.sendMessage("refresh_all_computers_list")
        self.stdout("Computer<ip={0}> changed to KnownComputer".format(known_computer.ip))

    @inside_func
    def computer_attribute_command(self, computer, command):
        try:
            attr = getattr(computer, command)
        except AttributeError:
            self.stdout("No Such command")
            return None
        else:
            if inspect.ismethod(attr):
                output = attr()
                if output is not None:
                    self.stdout(output)
            else:
                self.stdout(attr)

    def run_shell(self, command):
        self = Attribute(self)  # Instead of using '@inside_func' since it causes error with pubsub.
        help_func = lambda *args, **kwargs: self.stdout(Network.HELP_STRING)
        commands = {"computers": self.print_computers,
                    "refresh": self.refresh,
                    "help": help_func}
        ip_commands = {"scan": self.scan,
                       "database": self.save_in_database,
                       "json": self.print_json,
                       "information": self.run_information_command,
                       "known": self.change_to_known}
        for i in xrange(1):
            args = map(lambda arg: arg.lower(), command.split())
            if len(args) == 0:  # If <Enter> was pressed.
                continue

            command = args[0]
            if command in commands.keys():
                commands[command](args)
                continue

            ip = args[0]
            if ip not in self.computers.keys():
                self.stdout("No Such IP or Command")
                self.stdout("Type 'help' for help")
                continue
            computer = self.computers[ip]

            if len(args) < 2:  # If there is no command.
                self.stdout("[IP] [command]")
                self.stdout("Type 'help' for help")
                continue

            command = args[1]
            if command in ip_commands.keys():
                ip_commands[command](computer, self, args)
                continue
            self.computer_attribute_command(computer, command)

    @inside_func
    def start_network(self, controller=None):
        pub.subscribe(self.run_shell, "stdin")
        self.computers = self.__get_computers()
        pub.sendMessage("initiate_window")
        self.__run_discover_known_computers_thread()
        self.__run_http_thread()
# ------------End of Class <Network>------------
# ------------End of Code------------


def print_string(st):
    print st

if __name__ == "__main__":
    network = Network(print_string)
    network.start_network()
    # 172.16.1.94
    # WS674606