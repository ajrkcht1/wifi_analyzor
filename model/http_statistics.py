from model.http.http_packet import HttpPacket
from model.decorators.inside_class_decorator import inside_func
from model.decorators.thread_safe_decorator import thread_safe_function


class Site(object):
    def __init__(self, url, http_object):
        self.url = url
        self.host = http_object.headers["host"] if "host" in http_object.headers.keys() else ""
        self.browser = str(http_object.headers["user-agent"]) if "user-agent" in http_object.headers.keys() else ""
        self.language = str(http_object.headers["accept-language"]) if "accept-language" in http_object.headers.keys() else ""
        self.encoding = str(http_object.headers["accept-encoding"]) if "accept-encoding" in http_object.headers.keys() else ""
        self.times = 1


#------------Start of Class <HttpStatistics>------------
class HttpStatistics(object):
    @inside_func
    def __init__(self, computer):
        """
        Parameters:
            Computer computer
        """
        self.computer = computer
        self.my_ip = computer.ip
        self.sites = list()
        self.hosts = list()
        self.total_packets = 0
        self.packets = list()

    @inside_func
    def add_packet(self, packet):
        """
            *Explanation*
            Get: packet
            Return type: None
        """
        http_packet = HttpPacket(packet)
        self.packets.append(http_packet)
        self.total_packets += 1
        if http_packet.type == HttpPacket.REQUEST and "path" in http_packet.http_object.__dict__.keys():
            url = http_packet.http_object.path
            if "host" in http_packet.http_object.headers.keys():
                url = http_packet.http_object.headers["host"] + http_packet.http_object.path
                self.hosts.append(http_packet.http_object.headers["host"])
            self.sites.append(Site(url, http_packet.http_object))
            print http_packet.http_object.headers.keys()


    @thread_safe_function
    def __getattribute__(self, item):
        return object.__getattribute__(self, item)
#------------End of Class <HttpStatistics>------------
#------------End of Code------------