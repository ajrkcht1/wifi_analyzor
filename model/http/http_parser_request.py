""""
    http://stackoverflow.com/questions/4685217/parse-raw-http-headers
"""
from BaseHTTPServer import BaseHTTPRequestHandler
from StringIO import StringIO
import os


class HttpRequest(BaseHTTPRequestHandler):
    """
    Examples:
        request = HttpRequest(http_request)
        if request.error_code is not None:
            request.error_code        # None  (check this first)
            request.error_message     # "Bad request syntax ('GET')"
        request.command,              # "GET"
        request.path,                 # "/who/ken/trust.html"
        request.request_version       # "HTTP/1.1"
        request.headers
    """
    def __init__(self, request_text):
        self.rfile = StringIO(request_text)
        self.raw_requestline = self.rfile.readline()
        self.error_code = self.error_message = None
        self.parse_request()

    def send_error(self, code, message):
        self.error_code = code
        self.error_message = message

    def __str__(self):
        self.to_string()

    def __repr__(self):
        self.to_string()

    def to_string(self):
        output = ""
        if self.error_code is not None:
            output += self.error_code + os.linsep
            output += self.error_message + os.linsep
        else:
            output += self.command + ' '
            output += self.path + ' '
            output += self.request_version + os.linesep
            output += str(self.headers)
        return output