from httplib import HTTPResponse
from StringIO import StringIO
import os


class HttpResponse(object):
    """
    Examples:
        response = HttpResponse(string)
        self.version
        self.status
        self.reason
        response.getheader("Content-Type")
        response.headers
        response.content
    """
    def __init__(self, load):
        self.load = load
        self.response = HTTPResponse(FakeSocket(load))
        self.response.begin()
        self.headers = dict(self.response.getheaders())
        self.content = self.response.read(len(self.load)) # The len here will give a 'big enough' value to read the whole content.

    def __str__(self):
        self.to_string()

    def __repr__(self):
        self.to_string()

    def to_string(self):
        output = ""
        output += "HTTP/" + str(self.version) + ' '
        output += str(self.status) + ' '
        output += self.reason + os.linesep
        for key, value in self.headers.iteritems():
            output += key + ': ' + value + os.linesep
        return output

    def __getattr__(self, item):
        """
            If attribute is not in HttpResponse class so take it from httplib.HTTPResponse class.
            Get: attribute
            Return type: func/value
        """
        return getattr(self.response, item)


class FakeSocket():
    def __init__(self, response_str):
        self._file = StringIO(response_str)

    def makefile(self, *args, **kwargs):
        return self._file