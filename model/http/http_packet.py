from model.http.http_parser_request import HttpRequest
from model.http.http_parser_respond import HttpResponse


class HttpPacket(object):
    RESPOND = "respond"
    REQUEST = "request"

    def __init__(self, packet):
        self.load = packet
        self.type, self.http_object = self.__get_http_object_and_type()

    def __get_http_object_and_type(self):
        if self.load.startswith("HTTP"):
            type = HttpPacket.RESPOND
            http_object = HttpResponse(self.load)
        else:
            type = HttpPacket.REQUEST
            http_object = HttpRequest(self.load)
        return type, http_object

    def __str__(self):
        return self.http_object.to_string()

    def __repr__(self):
        return self.http_object.to_string()

if __name__ == "__main__":
    http_request = "GET /st?sec=8765945&inHead=true&id=-7327356459158467872&ref=&sr=1920x1080&altip= HTTP/1.1\r\n" \
                   "Accept: application/javascript, */*;q=0.8\r\n" \
                   "Referer: http://www.walla.co.il/\r\n" \
                   "Accept-Language: he-IL\r\n" \
                   "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko\r\n" \
                   "Accept-Encoding: gzip, deflate\r\n" \
                   "Host: st.dynamicyield.com\r\n" \
                   "Connection: Keep-Alive\r\n" \
                   "Cookie: DYID=-7327356459158467872; DYSES=51d09af04dab31e768ca787864274636\r\n" \
                   "\r\n"

    http_response1 = """HTTP/1.1 200 OK\r\nDate: Thu, Jul  3 15:27:54 2014\r\nContent-Type: text/xml; charset="utf-8"\r\nConnection: close\r\nContent-Length: 626"""
    http_response2 = "HTTP/1.1 404 Not Found\r\n" \
                     "Date: Sun, 14 Feb 2016 13:46:14 GMT\r\n" \
                     "Server: Apache/2.4.12\r\n" \
                     "Content-Length: 330\r\n" \
                     "Keep-Alive: timeout=5\r\n" \
                     "Connection: Keep-Alive\r\n" \
                     "Content-Type: text/html; charset=iso-8859-1\r\n" \
                     "\r\n" \
                     """\<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">\r\n<html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL /css/tab_a.gif was not found on this server.</p><p>Additionally, a 404 Not Founderror was encountered while trying to use an ErrorDocument to handle the request.</p></body></html>" \\"""

    response = HttpPacket(http_response2)
    print response.http_object.__dict__.keys()
    request = HttpPacket(http_request)
    print request.http_object.__dict__.keys()