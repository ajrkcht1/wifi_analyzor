import os
from model.nmap_package.nmap_report import NmapReport


#------------Start of Class <Ports>------------
class Ports(object):
    def __init__(self, ip, cache_filename="ports.cache", opened_ports=list(), closed_ports=list()):
        """
        Parameters:
            [ String ip, String cache_filename="ports.cache", Tuple ports_range=(1,2**16), List<int> opened_ports=[],
              List<int> closed_ports=[] ]
        """
        self.ip = ip
        self.services = list()
        self.os_list = ["Unknown"]
        self.opened_ports = opened_ports
        self.closed_ports = closed_ports
        self.cache_filename = cache_filename

    def start_port_scan(self, database, ports_range=None):
        """
            *Explanation*
            Get:
            Return type: None
        """
        self.os_list, self.services = NmapReport.get_ports_of_computer(self.ip, ports_range=ports_range, database=database)
        if ports_range is None:
            if len(self.services) != 0:
                min_port, max_port = self.services[0].port, self.services[-1].port
            else:
                min_port, max_port = 0, 1024
        else:
            min_port, max_port = ports_range
        self.opened_ports = list()
        self.closed_ports = list()
        for port in xrange(min_port, max_port + 1):
            if port in [service.port for service in self.services]:
                self.opened_ports.append(port)
            else:
                self.closed_ports.append(port)

    def refresh_cache(self, database):
        """
            *Explanation*
            Get:
            Return type: None
        """
        self.start_port_scan(database=database)
        # TODO

    def to_string(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        output = ""
        if len(self.services) == 0:
            output = "No Port Results <Computer: ip='{ip}'>:".format(ip=self.ip)
            return output
        output += "Opened Ports <Computer: ip='{ip}'>:".format(ip=self.ip) + os.linesep
        for service in self.services:
            output += "  " + repr(service) + os.linesep
        output = output[:output.rfind(os.linesep)]  # Remove last linesep.
        return output

    def __str__(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        return self.to_string()

    def __repr__(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        return self.to_string()

    @property
    def __dict__(self):
        ports = {service.port: {"service": service.service,
                                "banner": service.banner,
                                "protocol": service.protocol,
                                "state": service.state}
                 for service in self.services}
        return ports
#------------End of Class <Ports>------------
#------------End of Code------------