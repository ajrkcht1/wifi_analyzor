import socket
import select
from model.logger.log import Log


def send(data, client_socket):
    data_len = len(data)
    total_sent = 0
    while total_sent < data_len:
        sent = client_socket.send(data[total_sent:])
        if sent == 0:
            raise RuntimeError("socket connection broken")
        total_sent = total_sent + sent


def receive(current_socket, message_len):
        chunks = []
        bytes_received = 0
        while bytes_received < message_len:
            chunk = current_socket.recv(min(message_len - bytes_received, 2048))
            if chunk == '':
                return chunk
                #raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_received += len(chunk)
        return ''.join(chunks)


class Server(object):
    MAX_LEN = 8
    CLIENTS_LISTEN = 5
    RECEIVE_SIZE = 1024
    LOGOUT = "Connection with client had lost."
    LOGIN = "Connection with client has been started."

    def __init__(self, ip, port, log=Log(class_name="Server").log):
        self.run = False
        self.ip = ip
        self.port = port
        self.messages_to_send = []
        self.open_client_sockets = []
        self.server_socket = socket.socket()

        self.rlist = list()
        self.wlist = list()
        self.xlist = list()

        self.log = log

    def open(self):
        self.run = True
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(Server.CLIENTS_LISTEN)

    def get_clients_status(self):
        return select.select([self.server_socket] + self.open_client_sockets, self.open_client_sockets, [])

    def is_sendable(self, current_socket):
        return current_socket is self.server_socket

    def add_client_and_get_him(self):
        if self.log is not None:
            self.log.debug(Server.LOGOUT)                                                          # Debug
        (new_socket, address) = self.server_socket.accept()
        self.open_client_sockets.append(new_socket)
        return new_socket

    def get_data(self, current_socket):
        data = receive(current_socket, Server.MAX_LEN)
        if data == "":
            return data
        message_len = int(data)
        return receive(current_socket, message_len)

    def delete_client(self, current_socket):
        if self.log is not None:
            self.log.debug(Server.LOGIN)                                                          # Debug
        self.open_client_sockets.remove(current_socket)

    def add_answer(self, client_socket, data, send_all=False):
        self.messages_to_send.append((client_socket, data, send_all))

    def send_answers(self, wlist):
        for message in self.messages_to_send:
            (client_socket, data, send_all) = message
            if self.log is not None:
                self.log.debug("Try to send " + data + " to " + str(client_socket))         # Debug
            sent = False
            for recv_socket in wlist:
                if send_all:
                    if recv_socket != client_socket:
                        send(data, recv_socket)
                        if self.log is not None:
                            self.log.debug("send " + data + " to " + str(recv_socket))      # Debug
                    sent = True
                elif recv_socket == client_socket:
                    if self.log is not None:
                        self.log.debug("send " + data + " to " + str(recv_socket))          # Debug
                    send(data, recv_socket)
                    sent = True
            if sent:
                self.messages_to_send.remove(message)

    def start(self, answer_func):
        self.open()
        while self.run:
            self.rlist, self.wlist, self.xlist = self.get_clients_status()
            for current_socket in self.rlist:
                if self.is_sendable(current_socket):
                    self.add_client_and_get_him()
                else:
                    try:
                        data = self.get_data(current_socket)
                    except socket.error:
                        self.delete_client(current_socket)
                    else:
                        if data == "":
                            self.delete_client(current_socket)
                        else:
                            if self.log is not None:
                                self.log.debug("Got: <{data}>".format(data=data))           # Debug
                            answer = answer_func(current_socket, data)
                            if answer is not None:
                                self.add_answer(current_socket, answer)
            self.send_answers(self.wlist)

    def close(self):
        self.server_socket.close()

if __name__ == "__main__":
    ip = "127.0.0.1"
    port = 8888
    answer_function = lambda client_socket, data: "ACK"
    server = Server(ip, port)
    server.start(answer_function)