"""
http://timgolden.me.uk/python/wmi/cookbook.html
"""
import os
import threading
import pythoncom
import time
import win32con
import wmi
import wx
from wx.lib.pubsub import pub


#------------Start of Class <Information>------------
class Information(object):
    WMI_USER = "user"
    SW_SHOWNORMAL = 1

    def __init__(self, computer):
        """
        Parameters:
            WMI wmi_handler
        """
        pythoncom.CoInitialize()
        self.computer = computer
        try:
            #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
            self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        except Exception as e:
            pub.sendMessage("warn", message="An error has occurred: Can't connect to this computer" + os.linesep
                                            + "Information: " + os.linesep + str(e), caption="Error")
            raise e
        self.name = self.wmi_handler.Win32_OperatingSystem()
        self.processes = None
        self.services = None
        self.processes_cpu = {}

    def start_monitoring(self, status="Start Monitoring..."):
        pythoncom.CoInitialize()
        start_bar = lambda: wx.CallAfter(pub.sendMessage, "start_progress_bar", title="WMI Scanning")
        update_status = lambda st: wx.CallAfter(pub.sendMessage, "update_status", status=st)
        update_bar = lambda value: wx.CallAfter(pub.sendMessage, "update_gauge_value", value=value)
        start_bar()
        update_status(status)
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        self.processes = self.wmi_handler.Win32_Process()
        update_bar(5000)
        self.services = self.wmi_handler.Win32_Service()
        update_bar(10000)
        pub.sendMessage("set_services", objs=self.services)
        threading.Thread(target=self.get_processes_cpu).start()

    def get_processes(self):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        self.processes = self.wmi_handler.Win32_Process()
        threading.Thread(target=self.get_processes_cpu).start()
        return self.processes

    def get_services(self):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        self.services = self.wmi_handler.Win32_Service()
        return self.services

    def refresh(self):
        threading.Thread(target=self.start_monitoring, kwargs={"status": "Refreshing..."}).start()

    def create_process(self, name):
        """
            For security reasons the Win32_Process.Create method cannot be used to start an interactive process remotely.
        """
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        process_startup = self.wmi_handler.Win32_ProcessStartup.new(ShowWindow=win32con.SW_SHOWNORMAL)
        process_id, return_value = self.wmi_handler.Win32_Process.Create(CommandLine=name, ProcessStartupInformation=process_startup)
        if return_value == 0:
            process = self.wmi_handler.Win32_Process(ProcessId=process_id)[0]
            self.processes.append(process)
        return process_id, return_value

    def kill_process(self, pid):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        self.wmi_handler.Win32_Process(ProcessId=pid)[0].Terminate()
        for i in xrange(len(self.processes)):
            if self.processes[i].ProcessId == pid:
                process = self.processes.pop(i)
                #process.Terminate()
                break
        pub.sendMessage("set_processes", objs=self.processes)
        pub.sendMessage("refresh_processes_list")

    def stop_service(self, name):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        for service in self.wmi_handler.Win32_Service(Name=name):
            result, = service.StopService()
            if result == 0:
                while True:
                    time.sleep(0.1)
                    new_service = self.wmi_handler.Win32_Service(Name=name)[0]
                    if new_service.state == "Stopped":
                        break
                for i in xrange(len(self.services)):
                    if self.services[i].Name == new_service.Name:
                        self.services[i] = new_service
                pub.sendMessage("set_services", objs=self.services)
                pub.sendMessage("refresh_services_list")
                pass    # Stopped
            elif result == 2:
                # Access-Error: Needs to Run 'As Administrator'
                pub.sendMessage("warn", message="Access-Error: Needs to Run 'As Administrator'.", caption="Error")
            else:
                # Unknown Error
                pub.sendMessage("warn", message="An error has occurred: Can't stop this service.", caption="Error")
            break
        else:
            print name, ">> Service not found."

    def start_service(self, name):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        for service in self.wmi_handler.Win32_Service(Name=name):
            result, = service.StartService()
            if result == 0:
                # Stopped
                while True:
                    time.sleep(0.1)
                    new_service = self.wmi_handler.Win32_Service(Name=name)[0]
                    if new_service.state == "Running":
                        break
                for i in xrange(len(self.services)):
                    if self.services[i].Name == new_service.Name:
                        self.services[i] = new_service
                pub.sendMessage("set_services", objs=self.services)
                pub.sendMessage("refresh_services_list")
            elif result == 2:
                # Access-Error: Needs to Run 'As Administrator'
                pub.sendMessage("warn", message="Access-Error: Needs to Run 'As Administrator'.", caption="Error")
            else:
                # Unknown Error
                pub.sendMessage("warn", message="An error has occurred: Can't start this service.", caption="Error")
            break
        else:
            print name, ">> Service not found."

    def refresh_service(self, name):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        new_service = self.wmi_handler.Win32_Service(Name=name)[0]
        for i in xrange(len(self.services)):
            if self.services[i].Name == new_service.Name:
                self.services[i] = new_service
        pub.sendMessage("set_services", objs=self.services)
        pub.sendMessage("refresh_services_list")

    def reboot_computer(self):
        """
            Require privileges: RemoteShutdown
        """
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        os = self.wmi_handler.Win32_OperatingSystem(Primary=1)[0]
        os.Reboot()

    def __get_cpu(self, pid):
        try:
            handle = self.wmi_handler.Win32_PerfRawData_PerfProc_Process(IDProcess=pid)[0]
            n0, d0 = long(handle.PercentProcessorTime), long(handle.Timestamp_Sys100NS)
            handle = self.wmi_handler.Win32_PerfRawData_PerfProc_Process(IDProcess=pid)[0]
            n1, d1 = long(handle.PercentProcessorTime), long(handle.Timestamp_Sys100NS)
        except IndexError:  # No Such Process-ID
            return 0.0
        try:
            percent_processor_time = (float(n1 - n0) / float(d1 - d0)) * 100.0
        except ZeroDivisionError:
            percent_processor_time = 0.0
        return percent_processor_time

    def get_processes_cpu(self):
        pythoncom.CoInitialize()
        #self.wmi_handler = wmi.WMI(computer=self.computer.ip, user=self.computer.name) # TODO connect by IP & username.
        self.wmi_handler = wmi.WMI(computer=self.computer.ip)  # WMI doesn't work in my house (Error: Access Denied) Using myself as wmi object
        update_status = lambda st: wx.CallAfter(pub.sendMessage, "update_status", status=st)
        update_bar = lambda value: wx.CallAfter(pub.sendMessage, "update_gauge_value", value=value)
        destroy_bar = lambda: wx.CallAfter(pub.sendMessage, "destroy_progress_bar")
        update_bar(0)
        update_status("Receiving Processes Information...")
        cpu_sum = 0.0
        bar = 0
        processes_cpu = {}
        for process in self.processes:
            processes_cpu[process.ProcessId] = self.__get_cpu(process.ProcessId)
            cpu_sum += processes_cpu[process.ProcessId]
            bar += 10000/len(self.processes)
            update_bar(bar)
        for pid, cpu in processes_cpu.iteritems():
            final_cpu = float(cpu) / float(cpu_sum) * 100.0 if cpu_sum != 0 else 0
            self.processes_cpu[pid] = final_cpu
        update_bar(10000)
        destroy_bar()
        pub.sendMessage("set_processes", objs=self.processes)
        return self.processes_cpu
#------------End of Class <Information>------------
#------------End of Code------------