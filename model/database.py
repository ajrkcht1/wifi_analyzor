import ast
from datetime import datetime
from elasticsearch import Elasticsearch
import jsonpickle
import wx
from wx.lib.pubsub import pub


class Database(object):
    DOC_TYPE = "computer"

    def __init__(self, handle=Elasticsearch(), index="computers"):
        self.handle = handle
        self.index = index
        self.delete_all_indices()
        self.delete()
        self.create()

    def add_computer(self, computer):
        write_shell = lambda st: wx.CallAfter(pub.sendMessage, "write_shell", output=st)
        frozen = ast.literal_eval(jsonpickle.encode(computer, unpicklable=False))
        frozen["opened ports"] = list()
        for port in frozen["ports"].keys():
            frozen["ports"][int(port)] = frozen["ports"].pop(port)
            frozen["opened ports"].append(int(port))
        write_shell("<Computer: ip={ip}> has been added to Computers Database.".format(ip=computer.ip))
        #write_shell("    {0}".format(frozen))
        self.handle.index(index=self.index, doc_type=Database.DOC_TYPE, body=frozen, id=frozen["ip"])    # TODO time

    def delete(self):
        self.handle.indices.delete(index=self.index, ignore=[400, 404])

    def create(self):
        self.handle.indices.create(index=self.index, ignore=400)

    def delete_all_indices(self, exceptional=[".kibana"]):
        indices = self.handle.indices.get_aliases().keys()
        map(lambda i: indices.remove(i), exceptional)
        map(self.handle.indices.delete, indices)

"""
database = Elasticsearch()
database.indices.delete(index='people', ignore=[400, 404])
data = {'name': 'Shachar Lavi', 'age': {'year': 1997, 'years': 18}, 'timestamp': datetime.now()}
#data = {'status': 'up', 'operating_system': ['Unknown'], 'vendor': 'Tp-link Technologies', 'name': 'Unknown', 'ip': '192.168.0.1', 'mac': '10:FE:ED:BB:DD:28', 'ports': "No Port Results"}
response = database.index(index="people", doc_type='person', id=1, body=data)
print "Debug: ", response
response = database.get(index="people", doc_type='person', id=1)
print "Debug: ", response
response = database.indices.refresh(index="people")
print "Debug: ", response
raw_input()
"""