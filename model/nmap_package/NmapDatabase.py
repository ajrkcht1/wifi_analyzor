import wx
from wx.lib.pubsub import pub
from datetime import datetime
from elasticsearch import Elasticsearch


class NmapDataBase(object):
    # ----------------Constant-Variables---------------
    DOC_TYPE = "NmapScan"
    # -------------------------------------------------

    def __init__(self, nmap_report, index=None, database=Elasticsearch()):
        """
            NmapReport nmap_report, [ String index, Elasticsearch database ]
        """
        self.database    = database
        self.nmap_report = nmap_report
        self.starttime   = datetime.fromtimestamp(int(self.nmap_report.started))
        self.index       = "nmapScan-{0}".format(self.starttime.strftime('%Y-%m-%d')) if index is None else index
        self.delete()
        self.create()

    def save_report_in_database(self):
        """
            For each computer it save_in_database() its scan results (port, ipv4...)
                host_data = {
                        "endtime",
                        "address",
                        "hostnames",
                        "ipv4",
                        "ipv6",
                        "mac",
                        "status",
                        "elapsed",
                        "country",
                        "vendor",
                        "product",
                        "port",
                        "protocol",
                        "service",
                        "state",
                        "type",
                        "service-data"
                        }
        """
        write_shell = lambda st: wx.CallAfter(pub.sendMessage, "write_shell", output=st)
        write_shell("Indexing to Nmap-Scans Database:")
        for host_results in self.nmap_report.get_scan_results(all_hosts=False):
            for result in host_results:
                self.database.index(index=self.index, doc_type=NmapDataBase.DOC_TYPE, body=result, timestamp=self.starttime.strftime('%Y-%m-%d'))
            if result["status"] == "up":
                write_shell('    ' + result["ipv4"])

    def delete(self):
        self.database.indices.delete(index=self.index, ignore=[400, 404])

    def create(self):
        self.database.indices.create(index=self.index, ignore=400)


def main():
    database = NmapDataBase(targets="192.168.0.*", verbose=True)
    database.save_report_in_database()
    #raw_input("Press <Enter> to Exit...")

if __name__ == "__main__":
    main()