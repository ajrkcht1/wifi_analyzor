import json
import os
from time import sleep
import wx
from datetime import datetime
from libnmap.parser import NmapParser
from libnmap.process import NmapProcess
from libnmap.reportjson import ReportDecoder
from wx.lib.pubsub import pub
from model.nmap_package.NmapDatabase import NmapDataBase


# ---------Getting-Data-From-Nmap-Scan-Functions-----
    #-------------Get-Host-Basic-Info---------
def get_host_basic_info(nmap_host):
    """
        Return a dic with the host's data.
        host_basic_info = {
                    "endtime",
                    "address",
                    "hostnames",
                    "ipv4",
                    "ipv6",
                    "mac",
                    "status",
                    "elapsed",
                    "country",
                    "vendor",
                    "product"
                    }
    """
    host_basic_info = {"starttime": datetime.fromtimestamp(int(nmap_host.starttime)) if len(nmap_host.starttime) else 0,
                       "endtime": datetime.fromtimestamp(int(nmap_host.endtime)) if len(nmap_host.endtime) else 0,
                       "address": nmap_host.address,
                       "hostnames": nmap_host.hostnames,
                       "ipv4": nmap_host.ipv4,
                       "ipv6": nmap_host.ipv6,
                       "mac": nmap_host.mac,
                       "status": nmap_host.status,
                       "elapsed": (int(nmap_host.endtime) - int(nmap_host.starttime)) if len(nmap_host.starttime) != 0 and len(nmap_host.endtime) != 0 else 0,
                       "country": __get_geoip_code(nmap_host.address)}
    host_basic_info.update(__get_os(nmap_host))
    return host_basic_info


def __get_geoip_code(address):
    """
        Return the country of a given address.
    """
    #gi = pygeoip.GeoIP('GeoIP.dat')
    #return gi.country_code_by_addr(address)
    return "Unknown"


def __get_os(nmap_host):
    """
        Return a dic of vendor and product of the computer os.
        os = {
            'vendor',
            'product'
            }
    """
    vendor  = "unknown"
    product = "unknown"
    if nmap_host.is_up() and nmap_host.os_fingerprinted:
        cpelist = nmap_host.os.os_cpelist()
        if len(cpelist):
            mcpe    = cpelist.pop()
            vendor  = mcpe.get_vendor()
            product =  mcpe.get_product()
    os = {'vendor': vendor, 'product': product}
    return os


    #--------------Get-Results--------------
def get_results(nmap_service):
    """
        Return a list of dics with the scan results.
        dic = {
                "port",
                "protocol",
                "service",
                "state",
                "type",
                "service-data"
                }
    """
    port_result        = __get_port_results(nmap_service)
    nse_script_results = __get_nse_script_results(nmap_service)
    scan_results       = [port_result] + nse_script_results
    return scan_results


def __get_port_results(nmap_service):
    """
        Return the port scan results as a dic.
    """
    port_results = {"port": nmap_service.port,
                    "protocol": nmap_service.protocol,
                    "service": nmap_service.service,
                    "state": nmap_service.state,
                    "type": 'port-scan',
                    "service-data": nmap_service.banner}
    return port_results


def __get_nse_script_results(nmap_service):
    """
        Return the nse script output/results as a list<dic>.
    """
    nse_results = []
    for nse_item in nmap_service.scripts_results:
        nse_result = {"port": nmap_service.port,
                      "protocol": nmap_service.protocol,
                      "state": nmap_service.state,
                      "type": "nse-script",
                      "service": nse_item['id'],
                      "service-data": nse_item['output']}
        nse_results.append(nse_result)
    return nse_results


# --------------------Nmap-Report-Functions----------------------
def __get_nmap_report_with_scan(targets, options, verbose=False, retry=1, nmap_path=None):
    update_status = lambda st: wx.CallAfter(pub.sendMessage, "update_status", status=st)
    update_bar = lambda value: wx.CallAfter(pub.sendMessage, "update_gauge_value", value=value)
    start_bar = lambda: wx.CallAfter(pub.sendMessage, "start_progress_bar", title="Nmap Scanning")
    destroy_bar = lambda: wx.CallAfter(pub.sendMessage, "destroy_progress_bar")
    write_shell = lambda st: wx.CallAfter(pub.sendMessage, "write_shell", output=st)
    start_bar()
    while retry >= 0:
        nmap_process = NmapProcess(targets, options, fqp=nmap_path)
        if not verbose:
            nmap_process.run()
        else:
            write_shell("Start Nmap Scan on {0}".format(targets))
            nmap_process.run_background()
            if nmap_process.is_running():
                last_status = nmap_process.current_task.name if nmap_process.current_task is not None else "Loading..."
                update_status(last_status)
            while nmap_process.is_running():
                task_name = nmap_process.current_task.name if nmap_process.current_task is not None else "Loading..."
                if task_name != last_status:
                    last_status = task_name
                    update_status(task_name)
                    update_bar(100*100)
                    sleep(0.1)
                #print("Nmap Scan running ({0}): ETC: {1} DONE: {2}%".format(task_name, nmap_process.etc, nmap_process.progress))
                update_bar(int(float(nmap_process.progress) * 100))
                sleep(0.1)

        nmap_report = NmapParser.parse(nmap_process.stdout)
        retry -= 1
        if nmap_report.hosts_up != 0:
            break
        elif verbose:
            write_shell("Retrying again Nmap Scan...")
            update_status("Retrying again Nmap Scan...")
            update_bar(0)
    destroy_bar()
    return nmap_report


def get_nmap_report(targets, options, xml_file, json_file, verbose, retry=1, nmap_path=r"\Nmap"):
    if targets is not None:
        return __get_nmap_report_with_scan(targets, options, verbose, retry, nmap_path)
    elif xml_file is not None:
        return NmapParser.parse_fromfile(xml_file)
    elif json_file is not None:
        return json.loads(json.load(json_file), cls=ReportDecoder)
    else:
        raise Exception("ParameterError: Didn't get Data parameter")


class NmapReport(object):
    def __init__(self, targets=None, options="-sS", xml_file=None, json_file=None, verbose=False, retry=1, nmap_path=os.path.join("Nmap", "nmap.exe")):
        """
            [ String targets, String options, String xml_file, String json_file, Boolean verbose, Int retry, String nmap_path ]
        """
        if nmap_path is not None:
            program_path = os.path.dirname(os.path.realpath(__file__))
            program_path = program_path[:program_path.rfind("wifi_analyzor") + len("wifi_analyzor")]
            nmap_path = os.path.join(program_path, nmap_path)
            nmap_path = nmap_path if os.path.exists(nmap_path) else None
        self.nmap_report = get_nmap_report(targets, options, xml_file, json_file, verbose, retry, nmap_path)

    def get_scan_results(self, all_hosts=False):
        """
        Return:
            For each host:
                List<host_data>
                    host_data = {
                            "endtime",
                            "address",
                            "hostnames",
                            "ipv4",
                            "ipv6",
                            "mac",
                            "status",
                            "elapsed",
                            "country",
                            "vendor",
                            "product",
                            "port",
                            "protocol",
                            "service",
                            "state",
                            "type",
                            "service-data"
                            }
        """
        for nmap_host in self.nmap_report.hosts:
            if nmap_host.is_up() or all_hosts:
                final_scan_results = []
                host_basic_info    = get_host_basic_info(nmap_host)
                for service in nmap_host.services:
                    for result in get_results(service):
                        result.update(host_basic_info)
                        final_scan_results.append(result)
                if not final_scan_results:
                    final_scan_results.append(host_basic_info)
                yield final_scan_results

    def __getattr__(self, item):
        """
            If attribute is not in NmapReport class so take it from self.nmap_report.
            Get: attribute
            Return type: func/value
        """
        return getattr(self.nmap_report, item)

    @staticmethod
    def get_computers_in_lan(lan, database, verbose=True, nmap_path=os.path.join("Nmap", "nmap.exe")):
        """
            Computers in LAN:
            http://security.stackexchange.com/questions/36198/how-to-find-live-hosts-on-my-network
            https://nmap.org/book/man-host-discovery.html
            Return:
                List<NmapHost>:
                    NmapHost.ipv4               >> '172.16.2.158'
                    NmapHost.mac                >> 'A0:39:F7:42:23:23'
                    NmapHost.status             >> 'up'
                    NmapHost.vendor             >> 'LG Electronics (Mobile Communications)'
        """
        no_port_scan = "-sn"
        tcp_ack_ping_scan = "-PA"
        ports = "21,22,25,3389"
        options = "{0} {1}".format(no_port_scan, tcp_ack_ping_scan + ports)   # "-sn -PA21,22,25,3389"
        report = NmapReport(targets=lan, options=options, verbose=verbose, nmap_path=nmap_path)
        #nmap_db = NmapDataBase(report, index="scans", database=database)
        #nmap_db.save_report_in_database()
        hosts = list()
        for host in report.hosts:
            if host.is_up():
                hosts.append(host)
        return hosts

    @staticmethod
    def get_ports_of_computer(computer_ip, database, ports_range=None, retry=1, nmap_path=os.path.join("Nmap", "nmap.exe")):
        """
            Computer's Ports.

            Return: (List<String os>, List<Service>)
                String os           >> 'microsoft, windows_7'

                Service.port        >> 445
                Service.protocol    >> 'tcp'
                Service.service     >> 'microsoft-ds'
                Service.state       >> 'open'
                Service.banner      >> 'product: Microsoft Windows XP microsoft-ds ostype: Windows XP'
                Service.scripts_results: List<nse_script>
                    nse_script.protocol         >> 'tcp'
                    nse_script.service          >> 'http-favicon'
                    nse_script.state            >> 'open'
                    nse_script.service-data     >> 'Unknown favicon MD5: 082559A7867CF27ACAB7E9867A8B320F'
                    .port                       >> 80
        """
        nmap_ports_range = "-F"     # Fast - the most common 1,000 ports.
        if ports_range is not None:
            min_port, max_port = ports_range
            nmap_ports_range = "-p{0}-{1}".format(min_port, max_port)
        tcp_ack_ping_scan = "-PA"
        ports = "21,22,25,3389"
        os_detection = "-A"
        no_ping_scan = "-Pn"
        options = "{0} {1} {2} {3}".format(no_ping_scan, os_detection, tcp_ack_ping_scan + ports, nmap_ports_range) # "-Pn -A -PA21,22,25,3389 -p1-1024"
        report = NmapReport(targets=computer_ip, options=options, verbose=True, retry=retry, nmap_path=nmap_path)
        nmap_db = NmapDataBase(report, index="scan-{ip}".format(ip=computer_ip), database=database)
        nmap_db.save_report_in_database()
        services = list()
        os_list = list()
        for host in report.hosts:
            if host.is_up():
                for service in host.services:
                    if service.open() or service.state == "unknown":
                        """
                        print service.port,
                        print service.protocol,
                        print service.service,
                        print service.state,
                        print service.banner
                        for nse_script in service.scripts_results:
                            print nse_script
                        print '-'*80
                        """
                        services.append(service)
            if host.os_fingerprinted:
                cpelist = host.os.os_cpelist()
                for os in cpelist:
                    vendor  = os.get_vendor()
                    product =  os.get_product()
                    os = "{0}, {1}".format(vendor, product)
                    os_list.append(os)
        os_list = list(set(os_list))
        return os_list, services

if __name__ == "__main__":
    #NmapReport.get_computers_in_lan("172.16.0.1/20")
    #NmapReport.get_ports_of_computer("172.16.1.105", ports_range=(1, 1024))
    pass