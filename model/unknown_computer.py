from model.computer import Computer


#------------Start of Class <UnknownComputer>------------
class UnknownComputer(Computer):
    def __init__(self, *args, **kwargs):
        """
        Parameters:
            String ip, String mac, [ String name="Unknown", String operating_system=None, Ports opened_ports=None].
        """
        super(UnknownComputer, self).__init__( *args, **kwargs)
#------------End of Class <UnknownComputer>------------
#------------End of Code------------