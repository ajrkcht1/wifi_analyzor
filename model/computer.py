import os
import webbrowser
from model.ports import Ports


#------------Start of Class <Computer>------------
class Computer(object):
    def __init__(self, ip, mac, status="Unknown", vendor="Unknown", name="Unknown", operating_system=None):
        """
        Parameters:
            String ip, String mac, [ String name="Unknown", String operating_system=None].
        """
        self.ip = ip
        self.mac = mac
        self.status = status
        self.vendor = vendor
        self.name = name
        self.ports = Ports(self.ip)
        self.operating_system = self.get_os() if operating_system is None else operating_system

    @staticmethod
    def from_computer(computer, known_computer):
        """
        Initialize Computer from a Computer / UnknownComputer instance.
        Get: Computer computer or UnknownComputer computer
        Return type: Computer
        """
        known_computer.ip = computer.ip
        known_computer.mac = computer.mac
        known_computer.status = computer.status
        known_computer.vendor = computer.vendor
        known_computer.name = computer.name
        known_computer.operating_system = computer.operating_system
        known_computer.ports = computer.ports

    def get_os(self):
        """
            *Explanation*
            Assume that self.ports had been initialed.
            Get:
            Return type: None
        """
        return self.ports.os_list

    def get_opened_ports(self, database, ports_range=None):
        """
            *Explanation*
            Get:
            Return type: Ports
        """
        self.refresh_opened_ports(database=database, ports_range=ports_range)
        return self.ports

    def refresh_opened_ports(self, database, ports_range=None):
        """
            *Explanation*
            Get:
            Return type: None
        """
        self.ports.start_port_scan(database=database, ports_range=ports_range)
        self.operating_system = self.ports.os_list

    def save_in_database(self, database):
        database.add_computer(self)

    def open_in_database(self):
        url = r"http://localhost:{kibana_port}/app/kibana#/doc/computers/computers/computer/{ip}".format(kibana_port="5601", ip=self.ip)
        webbrowser.open(url)

    def to_string(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        output = ""
        output += "ip: {0}".format(self.ip) + os.linesep
        output += "mac: {0}".format(self.mac) + os.linesep
        output += "status: {0}".format(self.status) + os.linesep
        output += "vendor: {0}".format(self.vendor) + os.linesep
        output += "name: {0}".format(self.name) + os.linesep
        output += "operating_system: {0}".format(self.operating_system) + os.linesep
        output += "opened_ports: {0}".format(self.ports)
        return output

    def __str__(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        return self.to_string()

    def __repr__(self):
        """
            *Explanation*
            Get:
            Return type: None
        """
        return self.to_string()
#------------End of Class <Computer>------------
#------------End of Code------------